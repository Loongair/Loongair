/**
 * Created by cy on 2016-12-28.
 */
laAir.controller('laAir_Custom_SelectLegCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "选择航段";
    $document[0].title = $scope.title;

    /**
     * 返回
     */
    $scope.btnBack = function () {
        $window.location.href = "/Others/Custom/SpecialMeal.html?param=" + new Base64().encode(JSON.stringify($scope.PassengerFlight));
    };
}]);
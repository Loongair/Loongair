/**无陪儿童申请须知
 * Created by panShuang on 2017/3/14.
 */
laAir.controller('laAir_specialServicel_child_infoCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "无陪儿童旅客介绍";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.SpecialPassengerType=2;
    $scope.is_Transport_ClassFlag = "Child";
    $scope.ToApply=function () {
        $window.location.href = "/Others/Custom/SpecialServiceChildApply.html?param=" + new Base64().encode(JSON.stringify($scope.SpecialPassengerType));
    }

}]);
/**孕妇旅客申请须知
 * Created by panShuang on 2017/3/14.
 */
laAir.controller('laAir_specialServicel_pregnant_infoCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "孕妇旅客介绍";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.SpecialPassengerType=3;
    $scope.is_Transport_ClassFlag = "Pregnant";
    $scope.ToApply=function () {
        $window.location.href = "/Others/Custom/SpecialWheel.html?param=" + new Base64().encode(JSON.stringify($scope.SpecialPassengerType));
    }

}]);
/**婴儿旅客申请须知
 * Created by panShuang on 2017/3/14.
 */
laAir.controller('laAir_specialServicel_baby_infoCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "婴儿旅客介绍";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.SpecialPassengerType=5;
    $scope.is_Transport_ClassFlag = "Baby";
    $scope.ToApply=function () {
        $window.location.href = "/Others/Custom/SpecialWheel.html?param=" + new Base64().encode(JSON.stringify($scope.SpecialPassengerType));
    }

}]);
/**病患旅客申请须知
 * Created by panShuang on 2017/3/14.
 */
laAir.controller('laAir_specialServicel_patient_infoCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "病患旅客介绍";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.SpecialPassengerType=4;
    $scope.is_Transport_ClassFlag = "Patient";
    $scope.ToApply=function () {
        $window.location.href = "/Others/Custom/SpecialWheel.html?param=" + new Base64().encode(JSON.stringify($scope.SpecialPassengerType));
    }

}]);
/**视觉听力障碍旅客申请须知
 * Created by panShuang on 2017/3/14.
 */
laAir.controller('laAir_specialServicel_Vision_hearing_infoCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "视力听觉障碍旅客介绍";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.SpecialPassengerType=6;
    $scope.is_Transport_ClassFlag = "Vision_hearing";
    $scope.ToApply=function () {
        $window.location.href = "/Others/Custom/SpecialWheel.html?param=" + new Base64().encode(JSON.stringify($scope.SpecialPassengerType));
    }

}]);
/**轮椅旅客申请须知
 * Created by panShuang on 2017/3/14.
 */
laAir.controller('laAir_specialServicel_wheel_infoCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "轮椅旅客介绍";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.SpecialPassengerType=1;
    $scope.is_Transport_ClassFlag = "wheelchair";
    $scope.ToApply=function () {
        $window.location.href = "/Others/Custom/SpecialWheel.html?param=" + new Base64().encode(JSON.stringify($scope.SpecialPassengerType));
    }

}]);
/**
 * 轮椅申请特殊旅客特殊服务
 *
 */
laAir.controller('laAir_specialServicel_Wheel_ApplyCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "申请轮椅旅客特殊服务";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.passengename;
    $scope.flightNo;
    $scope.startCity;
    $scope.endCity;
    $scope.flightData;
    $scope.Param;
    $scope.departure;
    $scope.arrive;
    $scope.AirplaneType;
    $scope.DepartureCH;
    $scope.DepartureCityCH;
    $scope.ArriveCH;
    $scope.ArriveCityCH;
    $scope.DepartureTime;
    $scope.ArriveTime;
    $scope.Cabin;
    $scope.TktStatus;

    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.Param = JSON.parse(new Base64().decode(params[i].substr(6)));
                    }
                    catch (e) {

                    }
                    break;
                }

            }
        }
    }

    if ($scope.Param != undefined && $scope.Param != null) {
        $scope.passengename=$scope.Param.PassengerName;
        $scope.flightNo=$scope.Param.FlightNo;
        $scope.startCity=$scope.Param.DepartureCH;
        $scope.endCity=$scope.Param.ArriveCH;
        $scope.flightData=$scope.Param.FlightData;
        $scope.departure=$scope.Param.Departure;
        $scope.arrive=$scope.Param.Arrive;
        $scope.AirplaneType=$scope.Param.AirplaneType;
        $scope.DepartureCH=$scope.Param.DepartureCH;
        $scope.DepartureCityCH=$scope.Param.DepartureCityCH;
        $scope.ArriveCH=$scope.Param.ArriveCH;
        $scope.ArriveCityCH=$scope.Param.ArriveCityCH;
        $scope.DepartureTime=$scope.Param.DepartureTime;
        $scope.ArriveTime=$scope.Param.ArriveTime;
        $scope.Cabin=$scope.Param.Cabin;
        $scope.TktStatus=$scope.Param.TicketStatue;
    }
    $scope.btnQueryTicketClick = function () {
        var postdata = {
            TktStatus: $scope.TktStatus,
            PassengerName: $scope.passengename,
            PassengerAge: $("#customAge").val(),
            PassengerSex: $("#customSex").val(),
            PassengerFOIDNo: $("#customFildId").val(),
            PassengerFOIDType: $("#customFildType").val(),
            Departure:$scope.departure,
            Arrive:$scope.arrive,
            FlightNo: $scope.flightNo,
            ApplyPersonName: $("#signature").val(),
            FlightDate: $scope.flightData,
            SpecialPassengerType:1,
            AirplaneType:$scope.AirplaneType,//机型
            DepartureCH:$scope.DepartureCH,// 出发机场名称
            DepartureCityCH:$scope.DepartureCityCH,// 出发城市名称
            ArriveCH:$scope.ArriveCH,// 到达机场中文
            ArriveCityCH:$scope.ArriveCityCH,// 到达城市名称
            DepartureTime:$scope.DepartureTime,// 计划起飞时间
            ArriveTime:$scope.ArriveTime,// 计划到达时间
            Cabin:$scope.Cabin,// 舱位
            PickUpPerson: {},
            DropOffPerson: {},
            AccompanyPerson: [],
            SpecialPassengerDetail:""
        };
        var SpecialPassengerDetail = {
            WCD_WheeChairType: "",
            WCD_SelfHelp: "",
            WCD_Reason: "",
            WCD_SurgeryDate: "",
            WCD_SurgerySite: "",
            WCD_InjuryDegree: "",
            WCD_AccompanyCount: "",
            WheeChairType: "",
            WCD_Remark:"",
            WCD_IsConsignWheel:false,
            WCD_IsAutoWheel:false,
            WCD_IsDiagnoseCertificate:false
        };
        if($("#IsConsignWheel1").attr("checked")){
            SpecialPassengerDetail.WCD_IsConsignWheel=true;
        }else {
            SpecialPassengerDetail.WCD_IsConsignWheel=false;
        }
        if($("#IsAutoWheel1").attr("checked")){
            SpecialPassengerDetail.WCD_IsAutoWheel=true;
        }else {
            SpecialPassengerDetail.WCD_IsAutoWheel=false;
        }
        if($("#IsDiagnoseCertificate1").attr("checked")){
            SpecialPassengerDetail.WCD_IsDiagnoseCertificate=true;
        }else {
            SpecialPassengerDetail.WCD_IsDiagnoseCertificate=false;
        }
        $(".wheelchairType").each(function (i) {
            if ($(this).find('input').is(':checked')) {
                SpecialPassengerDetail.WheeChairType = SpecialPassengerDetail.WheeChairType + '-' + $(this).find('input').val();
                SpecialPassengerDetail.WCD_WheeChairType = SpecialPassengerDetail.WheeChairType;
            }

        });
        $(".Ability input[name=Question1]").each(function (i) {
            if (this.checked) {
                SpecialPassengerDetail.WCD_SelfHelp = $(this).val();
            }
            if (SpecialPassengerDetail.WCD_SelfHelp == "其他") {
                SpecialPassengerDetail.WCD_SelfHelp = SpecialPassengerDetail.WCD_SelfHelp + "-" + $("#qita2text").val();
            }
        });
        $(".Apply_reason_list input[name=Question2]").each(function (i) {
            if (this.checked) {
                SpecialPassengerDetail.WCD_Reason = $(this).val();
            }
            if (SpecialPassengerDetail.WCD_Reason == "手术") {
                SpecialPassengerDetail.WCD_AccompanyCount= $("#peers").val();
                SpecialPassengerDetail.WCD_SurgeryDate = $('#surgery_date').val();
                SpecialPassengerDetail.WCD_SurgerySite = $('#surgery_Parts').val();
                SpecialPassengerDetail.WCD_InjuryDegree = $('input:radio[name="injuryDegree"]:checked').val();
                SpecialPassengerDetail.WCD_Remark = $(".othertextarea").val();
            } else if (SpecialPassengerDetail.WCD_Reason == "其他") {
                SpecialPassengerDetail.WCD_Reason = SpecialPassengerDetail.WCD_Reason + "-" + $("#qita1text").val();
            }

        });
        //证件号码为空
        if (laGlobalLocalService.CheckStringIsEmpty($scope.passengeFildId)) {
            bootbox.alert('请输入乘机人证件号码');
            return;
        }
        //身份证号码错误
        if($("#customFildType").val()==1) {
            if (!laGlobalLocalService.IdentityCodeValid($scope.passengeFildId)) {
                bootbox.alert('乘机人的证件号码有误！请核对后再输入');
                return;
            }
        }
        if ($("#customAge").val()==""||$("#customAge").val()==undefined) {
            bootbox.alert('请输入乘机人的年龄');
            return;
        }
        if (SpecialPassengerDetail.WCD_WheeChairType==""||SpecialPassengerDetail.WCD_WheeChairType==undefined) {
            bootbox.alert('请选择轮椅类型');
            return;
        }
        if (SpecialPassengerDetail.WCD_SelfHelp==""||SpecialPassengerDetail.WCD_SelfHelp==undefined) {
            bootbox.alert('请选择乘机人是否有自理能力');
            return;
        }
        if (SpecialPassengerDetail.WCD_Reason==""||SpecialPassengerDetail.WCD_Reason==undefined) {
            bootbox.alert('请选择乘机人申请轮椅服务原因');
            return;
        }
        if (SpecialPassengerDetail.WCD_Reason=="手术") {
            if (SpecialPassengerDetail.WCD_SurgeryDate==""||SpecialPassengerDetail.WCD_Reason==undefined) {
                bootbox.alert('请选择乘机人手术时间');
                return;
            }
            if (SpecialPassengerDetail.WCD_SurgerySite==""||SpecialPassengerDetail.WCD_SurgerySite==undefined) {
                bootbox.alert('请填写乘机人手术部位');
                return;
            }
            if (SpecialPassengerDetail.WCD_InjuryDegree==""||SpecialPassengerDetail.WCD_InjuryDegree==undefined) {
                bootbox.alert('请选择乘机人受伤程度');
                return;
            }

        }
        postdata.SpecialPassengerDetail= JSON.stringify(SpecialPassengerDetail);
        if ($("#hasAccompany").attr("checked")) {
            $(".custom_Accompany_input").each(function (i) {
                var PassengerAccompany = {
                    "Name": "",
                    "Phone": "",
                    "FOIDNo": "",
                    "FOIDType": ""
                };
                    PassengerAccompany.Name = $("#" + "AccompanyName" + i).val();
                    PassengerAccompany.Phone = $("#" + "AccompanyPhone" + i).val();
                    PassengerAccompany.FOIDNo = $("#" + "AccompanyFildId" + i).val();
                    PassengerAccompany.FOIDType = $("#" + "AccompanyFildType" + i).val();
                    postdata.AccompanyPerson.push(PassengerAccompany);
                if (PassengerAccompany.Name==""||PassengerAccompany.Name==undefined) {
                    bootbox.alert('请填写随行人员'+(i+1)+'的姓名');
                    return;
                }
                if (PassengerAccompany.Phone==""||PassengerAccompany.Phone==undefined) {
                    bootbox.alert('请填写随行人员'+(i+1)+'的联系方式');
                    return;
                }

                if (PassengerAccompany.FOIDNo==""||PassengerAccompany.FOIDNo==undefined) {
                    bootbox.alert('请填写随行人员'+(i+1)+'的证件号码');
                    return;
                }
                if (PassengerAccompany.FOIDType==1) {
                    if (!laGlobalLocalService.IdentityCodeValid(PassengerAccompany.FOIDNo)) {
                        bootbox.alert('随行人员'+(i+1)+'的证件号码有误！请核对后再输入');
                        return;
                    }
                }
            });
        }

            var PickUpPerson = {
                "Name": "",
                "Relation": "",
                "Phone": "",
                "Email": ""
            };
            if($("#PickUpPersonhasAccompany").attr("checked")){
                PickUpPerson.Name=$("#PickUpPersonName").val();
                PickUpPerson.Relation=$("#PickUpPersonRela").val();
                PickUpPerson.Phone=$("#PickUpPersonPhone").val();
                PickUpPerson.Email=$("#PickUpPersonEmail").val();
                if (PickUpPerson.Name==""||PickUpPerson.Name==undefined) {
                    bootbox.alert('请填写送机人的姓名');
                    return;
                }
                if (PickUpPerson.Relation==""||PickUpPerson.Relation==undefined) {
                    bootbox.alert('请填写送机人和乘机人之间的关系');
                    return;
                }

                if (PickUpPerson.Phone==""||PickUpPerson.Phone==undefined) {
                    bootbox.alert('请填写送机人的联系电话');
                    return;
                }
                if (PickUpPerson.Email==""||PickUpPerson.Email==undefined) {
                    bootbox.alert('请填写送机人的电子邮箱');
                    return;
                }
                if (!laGlobalLocalService.CheckEMailFormat(PickUpPerson.Email)) {
                    bootbox.alert("请输入正确的送机人邮箱地址");
                    return;
                }
            }
            postdata.PickUpPerson=PickUpPerson;


            var DropOffPerson = {
                "Name": "",
                "Relation": "",
                "Phone": "",
                "Email": ""
            };
        if($("#PickUpPersonhasAccompany").attr("checked")){
            DropOffPerson.Name=$("#DropOffPersonName").val();
            DropOffPerson.Relation=$("#DropOffPersonRela").val();
            DropOffPerson.Phone=$("#DropOffPersonPhone").val();
            DropOffPerson.Email=$("#DropOffPersonEmail").val();
            if (DropOffPerson.Name==""||DropOffPerson.Name==undefined) {
                bootbox.alert('请填写接机人的姓名');
                return;
            }
            if (DropOffPerson.Relation==""||DropOffPerson.Relation==undefined) {
                bootbox.alert('请填写接机人和乘机人之间的关系');
                return;
            }

            if (DropOffPerson.Phone==""||DropOffPerson.Phone==undefined) {
                bootbox.alert('请填写接机人的联系电话');
                return;
            }
            if (DropOffPerson.Email==""||DropOffPerson.Email==undefined) {
                bootbox.alert('请填写接机人的电子邮箱');
                return;
            }
            if (!laGlobalLocalService.CheckEMailFormat(DropOffPerson.Email)) {
                bootbox.alert("请输入正确的接机人邮箱地址");
                return;
            }
        }
            postdata.DropOffPerson=DropOffPerson;
        if ($("#signature").val()==""||$("#signature").val()==undefined) {
            bootbox.alert('请申请人签名');
            return;
        }
        laUserService.ApplySpecialService(postdata, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == '0000') {
                $window.location.href = "/Others/Custom/SpecialServicesubmit.html?param=" + new Base64().encode(JSON.stringify(rs));
            }
            else {
                bootbox.alert(dataBack.Message);
            }
        })
    };
}]);
/**
 * 视觉听力申请特殊旅客特殊服务
 *
 */
laAir.controller('laAir_specialServicel_vision_hearing_ApplyCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "视力听力障碍旅客特殊服务";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.passengename;
    $scope.flightNo;
    $scope.startCity;
    $scope.endCity;
    $scope.flightData;
    $scope.Param;
    $scope.departure;
    $scope.arrive;
    $scope.AirplaneType;
    $scope.DepartureCH;
    $scope.DepartureCityCH;
    $scope.ArriveCH;
    $scope.ArriveCityCH;
    $scope.DepartureTime;
    $scope.ArriveTime;
    $scope.Cabin;
    $scope.TktStatus;


    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.Param = JSON.parse(new Base64().decode(params[i].substr(6)));
                    }
                    catch (e) {

                    }
                    break;
                }

            }
        }
    }

    if ($scope.Param != undefined && $scope.Param != null) {
        $scope.passengename=$scope.Param.PassengerName;
        $scope.flightNo=$scope.Param.FlightNo;
        $scope.startCity=$scope.Param.DepartureCH;
        $scope.endCity=$scope.Param.ArriveCH;
        $scope.flightData=$scope.Param.FlightData;
        $scope.departure=$scope.Param.Departure;
        $scope.arrive=$scope.Param.Arrive;
        $scope.AirplaneType=$scope.Param.AirplaneType;
        $scope.DepartureCH=$scope.Param.DepartureCH;
        $scope.DepartureCityCH=$scope.Param.DepartureCityCH;
        $scope.ArriveCH=$scope.Param.ArriveCH;
        $scope.ArriveCityCH=$scope.Param.ArriveCityCH;
        $scope.DepartureTime=$scope.Param.DepartureTime;
        $scope.ArriveTime=$scope.Param.ArriveTime;
        $scope.Cabin=$scope.Param.Cabin;
        $scope.TktStatus=$scope.Param.TicketStatue;
    }
    $scope.btnQueryTicketClick = function () {
        var postdata = {
            TktStatus:$scope.TktStatus,
            PassengerName: $scope.passengename,
            PassengerAge: $("#customAge").val(),
            PassengerSex: $("#customSex").val(),
            PassengerFOIDNo: $("#customFildId").val(),
            PassengerFOIDType: $("#customFildType").val(),
            Departure:$scope.departure,
            Arrive:$scope.arrive,
            FlightNo: $scope.flightNo,
            ApplyPersonName: $("#signature").val(),
            FlightDate: $scope.flightData,
            SpecialPassengerType:6,
            AirplaneType:$scope.AirplaneType,//机型
            DepartureCH:$scope.DepartureCH,// 出发机场名称
            DepartureCityCH:$scope.DepartureCityCH,// 出发城市名称
            ArriveCH:$scope.ArriveCH,// 到达机场中文
            ArriveCityCH:$scope.ArriveCityCH,// 到达城市名称
            DepartureTime:$scope.DepartureTime,// 计划起飞时间
            ArriveTime:$scope.ArriveTime,// 计划到达时间
            Cabin:$scope.Cabin,// 舱位
            PickUpPerson: {},
            DropOffPerson: {},
            AccompanyPerson: [],
            SpecialPassengerDetail:""
        };
        var SpecialPassengerDetail = {
            OPD_Detail: ""
        };
        $(".Ability input[name=Question1]").each(function (i) {
            if (this.checked) {
                SpecialPassengerDetail.OPD_Detail = $(this).val();
            }
        });
        postdata.SpecialPassengerDetail= JSON.stringify(SpecialPassengerDetail);
        //证件号码为空
        if (laGlobalLocalService.CheckStringIsEmpty($scope.passengeFildId)) {
            bootbox.alert('请输入乘机人证件号码');
            return;
        }
        //身份证号码错误
        if($("#customFildType").val()==1) {
            if (!laGlobalLocalService.IdentityCodeValid($scope.passengeFildId)) {
                bootbox.alert('乘机人的证件号码有误！请核对后再输入');
                return;
            }
        }
        if ($("#customAge").val()==""||$("#customAge").val()==undefined) {
            bootbox.alert('请输入乘机人的年龄');
            return;
        }
        if (SpecialPassengerDetail.OPD_Detail==""||SpecialPassengerDetail.OPD_Detail==undefined) {
            bootbox.alert('请选择特殊旅客类型');
            return;
        }
        if ($("#hasAccompany").attr("checked")) {
            $(".custom_Accompany_input").each(function (i) {
                var PassengerAccompany = {
                    "Name": "",
                    "Phone": "",
                    "FOIDNo": "",
                    "FOIDType": ""
                };
                PassengerAccompany.Name = $("#" + "AccompanyName" + i).val();
                PassengerAccompany.Phone = $("#" + "AccompanyPhone" + i).val();
                PassengerAccompany.FOIDNo = $("#" + "AccompanyFildId" + i).val();
                PassengerAccompany.FOIDType = $("#" + "AccompanyFildType" + i).val();
                postdata.AccompanyPerson.push(PassengerAccompany);
                if (PassengerAccompany.Name==""||PassengerAccompany.Name==undefined) {
                    bootbox.alert('请填写随行人员'+(i+1)+'的姓名');
                    return;
                }
                if (PassengerAccompany.Phone==""||PassengerAccompany.Phone==undefined) {
                    bootbox.alert('请填写随行人员'+(i+1)+'的联系方式');
                    return;
                }

                if (PassengerAccompany.FOIDNo==""||PassengerAccompany.FOIDNo==undefined) {
                    bootbox.alert('请填写随行人员'+(i+1)+'的证件号码');
                    return;
                }
                if (PassengerAccompany.FOIDType==1) {
                    if (!laGlobalLocalService.IdentityCodeValid(PassengerAccompany.FOIDNo)) {
                        bootbox.alert('随行人员' + (i + 1) + '的证件号码有误！请核对后再输入');
                        return;
                    }
                }
            });
        }

        var PickUpPerson = {
            "Name": "",
            "Relation": "",
            "Phone": "",
            "Email": ""
        };
        if($("#PickUpPersonhasAccompany").attr("checked")){
            PickUpPerson.Name=$("#PickUpPersonName").val();
            PickUpPerson.Relation=$("#PickUpPersonRela").val();
            PickUpPerson.Phone=$("#PickUpPersonPhone").val();
            PickUpPerson.Email=$("#PickUpPersonEmail").val();
            if (PickUpPerson.Name==""||PickUpPerson.Name==undefined) {
                bootbox.alert('请填写送机人的姓名');
                return;
            }
            if (PickUpPerson.Relation==""||PickUpPerson.Relation==undefined) {
                bootbox.alert('请填写送机人和乘机人之间的关系');
                return;
            }

            if (PickUpPerson.Phone==""||PickUpPerson.Phone==undefined) {
                bootbox.alert('请填写送机人的联系电话');
                return;
            }
            if (PickUpPerson.Email==""||PickUpPerson.Email==undefined) {
                bootbox.alert('请填写送机人的电子邮箱');
                return;
            }
            if (!laGlobalLocalService.CheckEMailFormat(PickUpPerson.Email)) {
                bootbox.alert("请输入正确的送机人邮箱地址");
                return;
            }
        }
        postdata.PickUpPerson=PickUpPerson;


        var DropOffPerson = {
            "Name": "",
            "Relation": "",
            "Phone": "",
            "Email": ""
        };
        if($("#PickUpPersonhasAccompany").attr("checked")){
            DropOffPerson.Name=$("#DropOffPersonName").val();
            DropOffPerson.Relation=$("#DropOffPersonRela").val();
            DropOffPerson.Phone=$("#DropOffPersonPhone").val();
            DropOffPerson.Email=$("#DropOffPersonEmail").val();
            if (DropOffPerson.Name==""||DropOffPerson.Name==undefined) {
                bootbox.alert('请填写接机人的姓名');
                return;
            }
            if (DropOffPerson.Relation==""||DropOffPerson.Relation==undefined) {
                bootbox.alert('请填写接机人和乘机人之间的关系');
                return;
            }

            if (DropOffPerson.Phone==""||DropOffPerson.Phone==undefined) {
                bootbox.alert('请填写接机人的联系电话');
                return;
            }
            if (DropOffPerson.Email==""||DropOffPerson.Email==undefined) {
                bootbox.alert('请填写接机人的电子邮箱');
                return;
            }
            if (!laGlobalLocalService.CheckEMailFormat(DropOffPerson.Email)) {
                bootbox.alert("请输入正确的接机人邮箱地址");
                return;
            }
        }
        postdata.DropOffPerson=DropOffPerson;
        if ($("#signature").val()==""||$("#signature").val()==undefined) {
            bootbox.alert('请申请人签名');
            return;
        }

        laUserService.ApplySpecialService(postdata, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == '0000') {
                $window.location.href = "/Others/Custom/SpecialServicesubmit.html?param=" + new Base64().encode(JSON.stringify(rs));
            }
            else {
                bootbox.alert(dataBack.Message);
            }
        })
    };
}]);
/**
 * 病患旅客申请特殊旅客特殊服务
 *
 */
laAir.controller('laAir_specialServicel_patient_ApplyCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "申请病患旅客特殊服务";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.passengename;
    $scope.flightNo;
    $scope.startCity;
    $scope.endCity;
    $scope.flightData;
    $scope.Param;
    $scope.departure;
    $scope.arrive;
    $scope.AirplaneType;
    $scope.DepartureCH;
    $scope.DepartureCityCH;
    $scope.ArriveCH;
    $scope.ArriveCityCH;
    $scope.DepartureTime;
    $scope.ArriveTime;
    $scope.Cabin;
    $scope.TktStatus;

    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.Param = JSON.parse(new Base64().decode(params[i].substr(6)));
                    }
                    catch (e) {

                    }
                    break;
                }

            }
        }
    }

    if ($scope.Param != undefined && $scope.Param != null) {
        $scope.TktStatus=$scope.Param.TicketStatue;
        $scope.passengename=$scope.Param.PassengerName;
        $scope.flightNo=$scope.Param.FlightNo;
        $scope.startCity=$scope.Param.DepartureCH;
        $scope.endCity=$scope.Param.ArriveCH;
        $scope.flightData=$scope.Param.FlightData;
        $scope.departure=$scope.Param.Departure;
        $scope.arrive=$scope.Param.Arrive;
        $scope.AirplaneType=$scope.Param.AirplaneType;
        $scope.DepartureCH=$scope.Param.DepartureCH;
        $scope.DepartureCityCH=$scope.Param.DepartureCityCH;
        $scope.ArriveCH=$scope.Param.ArriveCH;
        $scope.ArriveCityCH=$scope.Param.ArriveCityCH;
        $scope.DepartureTime=$scope.Param.DepartureTime;
        $scope.ArriveTime=$scope.Param.ArriveTime;
        $scope.Cabin=$scope.Param.Cabin;
    }
    $scope.btnQueryTicketClick = function () {
        var postdata = {
            TktStatus:$scope.TktStatus,
            PassengerName: $scope.passengename,
            PassengerAge: $("#customAge").val(),
            PassengerSex: $("#customSex").val(),
            PassengerFOIDNo: $("#customFildId").val(),
            PassengerFOIDType: $("#customFildType").val(),
            Departure:$scope.departure,
            Arrive:$scope.arrive,
            FlightNo: $scope.flightNo,
            ApplyPersonName: $("#signature").val(),
            FlightDate: $scope.flightData,
            SpecialPassengerType:4,
            AirplaneType:$scope.AirplaneType,//机型
            DepartureCH:$scope.DepartureCH,// 出发机场名称
            DepartureCityCH:$scope.DepartureCityCH,// 出发城市名称
            ArriveCH:$scope.ArriveCH,// 到达机场中文
            ArriveCityCH:$scope.ArriveCityCH,// 到达城市名称
            DepartureTime:$scope.DepartureTime,// 计划起飞时间
            ArriveTime:$scope.ArriveTime,// 计划到达时间
            Cabin:$scope.Cabin,// 舱位
            PickUpPerson: {},
            DropOffPerson: {},
            AccompanyPerson: [],
            SpecialPassengerDetail:""
        };
        //证件号码为空
        if (laGlobalLocalService.CheckStringIsEmpty($scope.passengeFildId)) {
            bootbox.alert('请输入乘机人证件号码');
            return;
        }
        //身份证号码错误
        if($("#customFildType").val()==1) {
            if (!laGlobalLocalService.IdentityCodeValid($scope.passengeFildId)) {
                bootbox.alert('乘机人的证件号码有误！请核对后再输入');
                return;
            }
        }
        if ($("#customAge").val()==""||$("#customAge").val()==undefined) {
            bootbox.alert('请输入乘机人的年龄');
            return;
        }
        if ($("#hasAccompany").attr("checked")) {
            $(".custom_Accompany_input").each(function (i) {
                var PassengerAccompany = {
                    "Name": "",
                    "Phone": "",
                    "FOIDNo": "",
                    "FOIDType": ""
                };
                PassengerAccompany.Name = $("#" + "AccompanyName" + i).val();
                PassengerAccompany.Phone = $("#" + "AccompanyPhone" + i).val();
                PassengerAccompany.FOIDNo = $("#" + "AccompanyFildId" + i).val();
                PassengerAccompany.FOIDType = $("#" + "AccompanyFildType" + i).val();
                postdata.AccompanyPerson.push(PassengerAccompany)
                if (PassengerAccompany.Name==""||PassengerAccompany.Name==undefined) {
                    bootbox.alert('请填写随行人员'+(i+1)+'的姓名');
                    return;
                }
                if (PassengerAccompany.Phone==""||PassengerAccompany.Phone==undefined) {
                    bootbox.alert('请填写随行人员'+(i+1)+'的联系方式');
                    return;
                }

                if (PassengerAccompany.FOIDNo==""||PassengerAccompany.FOIDNo==undefined) {
                    bootbox.alert('请填写随行人员'+(i+1)+'的证件号码');
                    return;
                }
                if (PassengerAccompany.FOIDType==1) {
                    if (!laGlobalLocalService.IdentityCodeValid(PassengerAccompany.FOIDNo)) {
                        bootbox.alert('随行人员'+(i+1)+'的证件号码有误！请核对后再输入');
                        return;
                    }
                }
            });
        }

        var PickUpPerson = {
            "Name": "",
            "Relation": "",
            "Phone": "",
            "Email": ""
        };
        if($("#PickUpPersonhasAccompany").attr("checked")){
            PickUpPerson.Name=$("#PickUpPersonName").val();
            PickUpPerson.Relation=$("#PickUpPersonRela").val();
            PickUpPerson.Phone=$("#PickUpPersonPhone").val();
            PickUpPerson.Email=$("#PickUpPersonEmail").val();
            if (PickUpPerson.Name==""||PickUpPerson.Name==undefined) {
                bootbox.alert('请填写送机人的姓名');
                return;
            }
            if (PickUpPerson.Relation==""||PickUpPerson.Relation==undefined) {
                bootbox.alert('请填写送机人和乘机人之间的关系');
                return;
            }

            if (PickUpPerson.Phone==""||PickUpPerson.Phone==undefined) {
                bootbox.alert('请填写送机人的联系电话');
                return;
            }
            if (PickUpPerson.Email==""||PickUpPerson.Email==undefined) {
                bootbox.alert('请填写送机人的电子邮箱');
                return;
            }
            if (!laGlobalLocalService.CheckEMailFormat(PickUpPerson.Email)) {
                bootbox.alert("请输入正确的送机人邮箱地址");
                return;
            }
        }
        postdata.PickUpPerson=PickUpPerson;


        var DropOffPerson = {
            "Name": "",
            "Relation": "",
            "Phone": "",
            "Email": ""
        };
        if($("#PickUpPersonhasAccompany").attr("checked")){
            DropOffPerson.Name=$("#DropOffPersonName").val();
            DropOffPerson.Relation=$("#DropOffPersonRela").val();
            DropOffPerson.Phone=$("#DropOffPersonPhone").val();
            DropOffPerson.Email=$("#DropOffPersonEmail").val();
            if (DropOffPerson.Name==""||DropOffPerson.Name==undefined) {
                bootbox.alert('请填写接机人的姓名');
                return;
            }
            if (DropOffPerson.Relation==""||DropOffPerson.Relation==undefined) {
                bootbox.alert('请填写接机人和乘机人之间的关系');
                return;
            }

            if (DropOffPerson.Phone==""||DropOffPerson.Phone==undefined) {
                bootbox.alert('请填写接机人的联系电话');
                return;
            }
            if (DropOffPerson.Email==""||DropOffPerson.Email==undefined) {
                bootbox.alert('请填写接机人的电子邮箱');
                return;
            }
            if (!laGlobalLocalService.CheckEMailFormat(DropOffPerson.Email)) {
                bootbox.alert("请输入正确的接机人邮箱地址");
                return;
            }
        }
        postdata.DropOffPerson=DropOffPerson;
        if ($("#signature").val()==""||$("#signature").val()==undefined) {
            bootbox.alert('请申请人签名');
            return;
        }

        laUserService.ApplySpecialService(postdata, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == '0000') {
                $window.location.href = "/Others/Custom/SpecialServicesubmit.html?param=" + new Base64().encode(JSON.stringify(rs));
            }
            else {
                bootbox.alert(dataBack.Message);
            }
        })
    };
}]);
/**laAir_specialServicel_patient_ApplyCtl
 * 婴儿旅客申请特殊旅客特殊服务
 *
 */
laAir.controller('laAir_specialServicel_baby_ApplyCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "申请婴儿旅客特殊服务";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.passengename;
    $scope.flightNo;
    $scope.startCity;
    $scope.endCity;
    $scope.flightData;
    $scope.Param;
    $scope.departure;
    $scope.arrive;
    $scope.AirplaneType;
    $scope.DepartureCH;
    $scope.DepartureCityCH;
    $scope.ArriveCH;
    $scope.ArriveCityCH;
    $scope.DepartureTime;
    $scope.ArriveTime;
    $scope.Cabin;
    $scope.TktStatus;

    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.Param = JSON.parse(new Base64().decode(params[i].substr(6)));
                    }
                    catch (e) {

                    }
                    break;
                }

            }
        }
    }

    if ($scope.Param != undefined && $scope.Param != null) {
        $scope.TktStatus=$scope.Param.TicketStatue;
        $scope.passengename=$scope.Param.PassengerName;
        $scope.flightNo=$scope.Param.FlightNo;
        $scope.startCity=$scope.Param.DepartureCH;
        $scope.endCity=$scope.Param.ArriveCH;
        $scope.flightData=$scope.Param.FlightData;
        $scope.departure=$scope.Param.Departure;
        $scope.arrive=$scope.Param.Arrive;
        $scope.AirplaneType=$scope.Param.AirplaneType;
        $scope.DepartureCH=$scope.Param.DepartureCH;
        $scope.DepartureCityCH=$scope.Param.DepartureCityCH;
        $scope.ArriveCH=$scope.Param.ArriveCH;
        $scope.ArriveCityCH=$scope.Param.ArriveCityCH;
        $scope.DepartureTime=$scope.Param.DepartureTime;
        $scope.ArriveTime=$scope.Param.ArriveTime;
        $scope.Cabin=$scope.Param.Cabin;
    }
    $scope.btnQueryTicketClick = function () {
        var postdata = {
            TktStatus:$scope.TktStatus,
            PassengerName: $scope.passengename,
            PassengerAge: $("#customAge").val(),
            PassengerSex: $("#customSex").val(),
            PassengerFOIDNo: $("#customFildId").val(),
            PassengerFOIDType: $("#customFildType").val(),
            Departure:$scope.departure,
            Arrive:$scope.arrive,
            FlightNo: $scope.flightNo,
            ApplyPersonName: $("#signature").val(),
            FlightDate: $scope.flightData,
            SpecialPassengerType:5,
            AirplaneType:$scope.AirplaneType,//机型
            DepartureCH:$scope.DepartureCH,// 出发机场名称
            DepartureCityCH:$scope.DepartureCityCH,// 出发城市名称
            ArriveCH:$scope.ArriveCH,// 到达机场中文
            ArriveCityCH:$scope.ArriveCityCH,// 到达城市名称
            DepartureTime:$scope.DepartureTime,// 计划起飞时间
            ArriveTime:$scope.ArriveTime,// 计划到达时间
            Cabin:$scope.Cabin,// 舱位
            PickUpPerson: {},
            DropOffPerson: {},
            AccompanyPerson: [],
            SpecialPassengerDetail:""
        };
        var SpecialPassengerDetail = {
            IPD_InfantName: "",
            IPD_InfantAge:"",
            IPD_InfantFoid:""
        };
        SpecialPassengerDetail.IPD_InfantName = $("#InfantName").val();
        SpecialPassengerDetail.IPD_InfantAge = $("#InfantAge").val();
        SpecialPassengerDetail.IPD_InfantFoid = $("#InfantFoid").val();
        postdata.SpecialPassengerDetail= JSON.stringify(SpecialPassengerDetail);
        //身份证号码错误
        if($("#customFildType").val()==1) {
            if (!laGlobalLocalService.IdentityCodeValid($scope.passengeFildId)) {
                bootbox.alert('乘机人的证件号码有误！请核对后再输入');
                return;
            }
        }
        if ($("#customAge").val()==""||$("#customAge").val()==undefined) {
            bootbox.alert('请输入乘机人的年龄');
            return;
        }
        if (SpecialPassengerDetail.IPD_InfantName==""||SpecialPassengerDetail.IPD_InfantName==undefined) {
            bootbox.alert('请填写婴儿姓名');
            return;
        }
        if (SpecialPassengerDetail.IPD_InfantFoid==""||SpecialPassengerDetail.IPD_InfantFoid==undefined) {
            bootbox.alert('请填写婴儿证件号码');
            return;
        }
        if ($("#hasAccompany").attr("checked")) {
            $(".custom_Accompany_input").each(function (i) {
                var PassengerAccompany = {
                    "Name": "",
                    "Phone": "",
                    "FOIDNo": "",
                    "FOIDType": ""
                };
                PassengerAccompany.Name = $("#" + "AccompanyName" + i).val();
                PassengerAccompany.Phone = $("#" + "AccompanyPhone" + i).val();
                PassengerAccompany.FOIDNo = $("#" + "AccompanyFildId" + i).val();
                PassengerAccompany.FOIDType = $("#" + "AccompanyFildType" + i).val();
                postdata.AccompanyPerson.push(PassengerAccompany);
                if (PassengerAccompany.Name==""||PassengerAccompany.Name==undefined) {
                    bootbox.alert('请填写随行人员'+(i+1)+'的姓名');
                    return;
                }
                if (PassengerAccompany.Phone==""||PassengerAccompany.Phone==undefined) {
                    bootbox.alert('请填写随行人员'+(i+1)+'的联系方式');
                    return;
                }

                if (PassengerAccompany.FOIDNo==""||PassengerAccompany.FOIDNo==undefined) {
                    bootbox.alert('请填写随行人员'+(i+1)+'的证件号码');
                    return;
                }
                if (PassengerAccompany.FOIDType==1) {
                    if (!laGlobalLocalService.IdentityCodeValid(PassengerAccompany.FOIDNo)) {
                        bootbox.alert('随行人员'+(i+1)+'的证件号码有误！请核对后再输入');
                        return;
                    }
                }

            });
        }

        var PickUpPerson = {
            "Name": "",
            "Relation": "",
            "Phone": "",
            "Email": ""
        };
        if($("#PickUpPersonhasAccompany").attr("checked")){
            PickUpPerson.Name=$("#PickUpPersonName").val();
            PickUpPerson.Relation=$("#PickUpPersonRela").val();
            PickUpPerson.Phone=$("#PickUpPersonPhone").val();
            PickUpPerson.Email=$("#PickUpPersonEmail").val();
            if (PickUpPerson.Name==""||PickUpPerson.Name==undefined) {
                bootbox.alert('请填写送机人的姓名');
                return;
            }
            if (PickUpPerson.Relation==""||PickUpPerson.Relation==undefined) {
                bootbox.alert('请填写送机人和乘机人之间的关系');
                return;
            }

            if (PickUpPerson.Phone==""||PickUpPerson.Phone==undefined) {
                bootbox.alert('请填写送机人的联系电话');
                return;
            }
            if (PickUpPerson.Email==""||PickUpPerson.Email==undefined) {
                bootbox.alert('请填写送机人的电子邮箱');
                return;
            }
            if (!laGlobalLocalService.CheckEMailFormat(PickUpPerson.Email)) {
                bootbox.alert("请输入正确的送机人邮箱地址");
                return;
            }
        }
        postdata.PickUpPerson=PickUpPerson;


        var DropOffPerson = {
            "Name": "",
            "Relation": "",
            "Phone": "",
            "Email": ""
        };
        if($("#DropOffPersonhasAccompany").attr("checked")){
            DropOffPerson.Name=$("#DropOffPersonName").val();
            DropOffPerson.Relation=$("#DropOffPersonRela").val();
            DropOffPerson.Phone=$("#DropOffPersonPhone").val();
            DropOffPerson.Email=$("#DropOffPersonEmail").val();
            if (DropOffPerson.Name==""||DropOffPerson.Name==undefined) {
                bootbox.alert('请填写接机人的姓名');
                return;
            }
            if (DropOffPerson.Relation==""||DropOffPerson.Relation==undefined) {
                bootbox.alert('请填写接机人和乘机人之间的关系');
                return;
            }

            if (DropOffPerson.Phone==""||DropOffPerson.Phone==undefined) {
                bootbox.alert('请填写接机人的联系电话');
                return;
            }
            if (DropOffPerson.Email==""||DropOffPerson.Email==undefined) {
                bootbox.alert('请填写接机人的电子邮箱');
                return;
            }
            if (!laGlobalLocalService.CheckEMailFormat(DropOffPerson.Email)) {
                bootbox.alert("请输入正确的接机人邮箱地址");
                return;
            }
        }
        postdata.DropOffPerson=DropOffPerson;
        if ($("#signature").val()==""||$("#signature").val()==undefined) {
            bootbox.alert('请申请人签名');
            return;
        }

        laUserService.ApplySpecialService(postdata, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == '0000') {
                $window.location.href = "/Others/Custom/SpecialServicesubmit.html?param=" + new Base64().encode(JSON.stringify(rs));
            }
            else {
                bootbox.alert(dataBack.Message);
            }
        })
    };
}]);
/**
 * 孕妇旅客申请特殊旅客特殊服务
 *
 */
var pregnant = angular.module('pregnant', ['laAir','ngFileUpload']);
pregnant.controller('laAir_specialServicel_pregnant_ApplyCtl', ['$filter', '$interval','Upload', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval,Upload, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "申请孕妇旅客特殊服务";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.passengename;
    $scope.flightNo;
    $scope.startCity;
    $scope.endCity;
    $scope.flightData;
    $scope.Param;
    $scope.departure;
    $scope.arrive;
    $scope.AirplaneType;
    $scope.DepartureCH;
    $scope.DepartureCityCH;
    $scope.ArriveCH;
    $scope.ArriveCityCH;
    $scope.DepartureTime;
    $scope.ArriveTime;
    $scope.Cabin;
    $scope.TktStatus;

    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.Param = JSON.parse(new Base64().decode(params[i].substr(6)));
                    }
                    catch (e) {

                    }
                    break;
                }

            }
        }
    }

    if ($scope.Param != undefined && $scope.Param != null) {
        $scope.passengename=$scope.Param.PassengerName;
        $scope.flightNo=$scope.Param.FlightNo;
        $scope.startCity=$scope.Param.DepartureCH;
        $scope.endCity=$scope.Param.ArriveCH;
        $scope.flightData=$scope.Param.FlightData;
        $scope.departure=$scope.Param.Departure;
        $scope.arrive=$scope.Param.Arrive;
        $scope.AirplaneType=$scope.Param.AirplaneType;
        $scope.DepartureCH=$scope.Param.DepartureCH;
        $scope.DepartureCityCH=$scope.Param.DepartureCityCH;
        $scope.ArriveCH=$scope.Param.ArriveCH;
        $scope.ArriveCityCH=$scope.Param.ArriveCityCH;
        $scope.DepartureTime=$scope.Param.DepartureTime;
        $scope.ArriveTime=$scope.Param.ArriveTime;
        $scope.Cabin=$scope.Param.Cabin;
        $scope.TktStatus=$scope.Param.TicketStatue;
    }
    //$scope.fileinfo=$scope.refund.file;
    $scope.btnQueryTicketClick = function () {

        Upload.upload({
            //服务端接收
            url: '/Flight/UploadFile',
            //上传的同时带的参数
            data: {file: document.getElementById("Pregnant_file").files }
        }).success(function (data, status, headers, config) {
            bootbox.alert("yes！");
        }).error(function (data, status, headers, config) {
            bootbox.alert("失败！");
        });

        var postdata = {
            TktStatus:$scope.TktStatus,
            PassengerName: $scope.passengename,
            PassengerAge: $("#customAge").val(),
            PassengerSex: $("#customSex").val(),
            PassengerFOIDNo: $("#customFildId").val(),
            PassengerFOIDType: $("#customFildType").val(),
            Departure:$scope.departure,
            Arrive:$scope.arrive,
            FlightNo: $scope.flightNo,
            ApplyPersonName: $("#signature").val(),
            FlightDate: $scope.flightData,
            SpecialPassengerType:3,
            AirplaneType:$scope.AirplaneType,//机型
            DepartureCH:$scope.DepartureCH,// 出发机场名称
            DepartureCityCH:$scope.DepartureCityCH,// 出发城市名称
            ArriveCH:$scope.ArriveCH,// 到达机场中文
            ArriveCityCH:$scope.ArriveCityCH,// 到达城市名称
            DepartureTime:$scope.DepartureTime,// 计划起飞时间
            ArriveTime:$scope.ArriveTime,// 计划到达时间
            Cabin:$scope.Cabin,// 舱位
            PickUpPerson: {},
            DropOffPerson: {},
            AccompanyPerson: [],
            SpecialPassengerDetail:""
        };
        var SpecialPassengerDetail = {
            PPD_PregnantWeek: ""
        };
        $(".Ability input[name=Question1]").each(function (i) {
            if (this.checked) {
                SpecialPassengerDetail.PPD_PregnantWeek = $(this).val();
            }

        });
        postdata.SpecialPassengerDetail= JSON.stringify(SpecialPassengerDetail);
        //证件号码为空
        if (laGlobalLocalService.CheckStringIsEmpty($scope.passengeFildId)) {
            bootbox.alert('请输入乘机人证件号码');
            return;
        }
        //身份证号码错误
        if($("#customFildType").val()==1) {
            if (!laGlobalLocalService.IdentityCodeValid($scope.passengeFildId)) {
                bootbox.alert('乘机人的证件号码有误！请核对后再输入');
                return;
            }
        }
        if ($("#customAge").val()==""||$("#customAge").val()==undefined) {
            bootbox.alert('请输入乘机人的年龄');
            return;
        }
        if (SpecialPassengerDetail.PPD_PregnantWeek==""||SpecialPassengerDetail.PPD_PregnantWeek==undefined) {
            bootbox.alert('请选择孕妇旅客类型');
            return;
        }
        var PickUpPerson = {
            "Name": "",
            "Relation": "",
            "Phone": "",
            "Email": ""
        };
        var DropOffPerson = {
            "Name": "",
            "Relation": "",
            "Phone": "",
            "Email": ""
        };
        if(SpecialPassengerDetail.PPD_PregnantWeek==2){
            if ($("#hasAccompany").attr("checked")) {
                $(".custom_Accompany_input").each(function (i) {
                    var PassengerAccompany = {
                        "Name": "",
                        "Phone": "",
                        "FOIDNo": "",
                        "FOIDType": ""
                    };
                    PassengerAccompany.Name = $("#" + "AccompanyName" + i).val();
                    PassengerAccompany.Phone = $("#" + "AccompanyPhone" + i).val();
                    PassengerAccompany.FOIDNo = $("#" + "AccompanyFildId" + i).val();
                    PassengerAccompany.FOIDType = $("#" + "AccompanyFildType" + i).val();
                    postdata.AccompanyPerson.push(PassengerAccompany);
                    if (PassengerAccompany.Name==""||PassengerAccompany.Name==undefined) {
                        bootbox.alert('请填写随行人员'+(i+1)+'的姓名');
                        return;
                    }
                    if (PassengerAccompany.Phone==""||PassengerAccompany.Phone==undefined) {
                        bootbox.alert('请填写随行人员'+(i+1)+'的联系方式');
                        return;
                    }

                    if (PassengerAccompany.FOIDNo==""||PassengerAccompany.FOIDNo==undefined) {
                        bootbox.alert('请填写随行人员'+(i+1)+'的证件号码');
                        return;
                    }
                    if (PassengerAccompany.FOIDType==1) {
                        if (!laGlobalLocalService.IdentityCodeValid(PassengerAccompany.FOIDNo)) {
                            bootbox.alert('随行人员'+(i+1)+'的证件号码有误！请核对后再输入');
                            return;
                        }
                    }

                });
            }


            if($("#PickUpPersonhasAccompany").attr("checked")){
                PickUpPerson.Name=$("#PickUpPersonName").val();
                PickUpPerson.Relation=$("#PickUpPersonRela").val();
                PickUpPerson.Phone=$("#PickUpPersonPhone").val();
                PickUpPerson.Email=$("#PickUpPersonEmail").val();
                if (PickUpPerson.Name==""||PickUpPerson.Name==undefined) {
                    bootbox.alert('请填写送机人的姓名');
                    return;
                }
                if (PickUpPerson.Relation==""||PickUpPerson.Relation==undefined) {
                    bootbox.alert('请填写送机人和乘机人之间的关系');
                    return;
                }

                if (PickUpPerson.Phone==""||PickUpPerson.Phone==undefined) {
                    bootbox.alert('请填写送机人的联系电话');
                    return;
                }
                if (PickUpPerson.Email==""||PickUpPerson.Email==undefined) {
                    bootbox.alert('请填写送机人的电子邮箱');
                    return;
                }
                if (!laGlobalLocalService.CheckEMailFormat(PickUpPerson.Email)) {
                    bootbox.alert("请输入正确的送机人邮箱地址");
                    return;
                }
            }
            postdata.PickUpPerson=PickUpPerson;

            if($("#DropOffPersonhasAccompany").attr("checked")){
                DropOffPerson.Name=$("#DropOffPersonName").val();
                DropOffPerson.Relation=$("#DropOffPersonRela").val();
                DropOffPerson.Phone=$("#DropOffPersonPhone").val();
                DropOffPerson.Email=$("#DropOffPersonEmail").val();
                if (DropOffPerson.Name==""||DropOffPerson.Name==undefined) {
                    bootbox.alert('请填写接机人的姓名');
                    return;
                }
                if (DropOffPerson.Relation==""||DropOffPerson.Relation==undefined) {
                    bootbox.alert('请填写接机人和乘机人之间的关系');
                    return;
                }

                if (DropOffPerson.Phone==""||DropOffPerson.Phone==undefined) {
                    bootbox.alert('请填写接机人的联系电话');
                    return;
                }
                if (DropOffPerson.Email==""||DropOffPerson.Email==undefined) {
                    bootbox.alert('请填写接机人的电子邮箱');
                    return;
                }
                if (!laGlobalLocalService.CheckEMailFormat(DropOffPerson.Email)) {
                    bootbox.alert("请输入正确的接机人邮箱地址");
                    return;
                }
            }
            postdata.DropOffPerson=DropOffPerson;
        }

        if ($("#signature").val()==""||$("#signature").val()==undefined) {
            bootbox.alert('请申请人签名');
            return;
        }
        Upload.upload({
            //服务端接收
            url: 'http://172.25.1.13:8085/Flight/UploadFile',
            //上传的同时带的参数
            data: {file: document.getElementById("Pregnant_file").files }
        }).success(function (data, status, headers, config) {
            laUserService.ApplySpecialService(postdata, function (dataBack, status) {
                var rs = dataBack;
                if (rs.Code == '0000') {
                    $window.location.href = "/Others/Custom/SpecialServicesubmit.html?param=" + new Base64().encode(JSON.stringify(rs));
                }
                else {
                    bootbox.alert(dataBack.Message);
                }
            })
        }).error(function (data, status, headers, config) {
            bootbox.alert("失败！");
        });

    };
}]);
/**
 * 无陪儿童申请特殊旅客特殊服务
 *
 */
laAir.controller('laAir_specialServicel_child_ApplyCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService','laFlightService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService,laFlightService, laGlobalLocalService) {
    $scope.title = "申请无陪儿童旅客特殊服务";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.passengename;
    $scope.flightNo;
    $scope.startCity;
    $scope.endCity;
    $scope.flightData;
    $scope.Param;
    $scope.departure;
    $scope.arrive;
    $scope.AirplaneType;
    $scope.DepartureCH;
    $scope.DepartureCityCH;
    $scope.ArriveCH;
    $scope.ArriveCityCH;
    $scope.DepartureTime;
    $scope.ArriveTime;
    $scope.Cabin;
    $scope.TktStatus;
    $scope.FlightInfo={C1:"",C2:"",T:"",F:"",Q:""};
    $scope.FliDynamic={};
    var td = new Date();
    td = new Date(td.setDate(td.getDate() + 4));
    var tdmm = (parseInt(td.getMonth() + 1)).toString();
    var tdday = td.getDate().toString();
    tdmm = (tdmm.length < 2) ? '0' + tdmm : tdmm;
    tdday = (tdday.length < 2) ? '0' + tdday : tdday;
    $scope.flightData=td.getFullYear() + '-' + tdmm + '-' + tdday;
    laUserService.FillCityAirportInfo(new Array("startCity", "endCity"), function () {
        var defCity = {"s": {"c": "HGH", "n": "杭州"}, "e": {"c": "CAN", "n": "广州"}};
        $("#startCity").attr("segnum", defCity.s.c);
        $("#startCity").val(defCity.s.n);
        $("#endCity").attr("segnum", defCity.e.c);
        $("#endCity").val(defCity.e.n);
    });
    $scope.btnQueryTicketClick = function () {
        $scope.DepartureCityCH=$scope.startCity=$("#startCity").val();
        $scope.ArriveCityCH=$scope.endCity=$("#endCity").val();
        $scope.departure=laUserService.SearchCityCodeByCityName($scope.DepartureCityCH);
        $scope.arrive=laUserService.SearchCityCodeByCityName($scope.ArriveCityCH);
        $scope.FlightInfo={C1:$scope.departure,C2:$scope.arrive,T:$scope.flightData,F:$scope.flightNo.toUpperCase(),Q:"0"};
        if ($scope.FlightInfo != undefined && $scope.FlightInfo != null) {
            QueryFlightDynamic();
        }
        var postdata = {
            TktStatus:'',
            PassengerName: $scope.passengename,
            PassengerAge: $("#customAge").val(),
            PassengerSex: $("#customSex").val(),
            PassengerFOIDNo: $("#customFildId").val(),
            PassengerFOIDType: $("#customFildType").val(),
            Departure:$scope.departure,
            Arrive:$scope.arrive,
            FlightNo: $scope.flightNo,
            ApplyPersonName: $("#signature").val(),
            FlightDate: $scope.flightData,
            SpecialPassengerType:2,
            AirplaneType:"",//机型
            DepartureCH:$scope.DepartureCH,// 出发机场名称
            DepartureCityCH:$scope.DepartureCityCH,// 出发城市名称
            ArriveCH:$scope.ArriveCH,// 到达机场中文
            ArriveCityCH:$scope.ArriveCityCH,// 到达城市名称
            DepartureTime:$scope.DepartureTime,// 计划起飞时间
            ArriveTime:$scope.ArriveTime,// 计划到达时间
            Cabin:$scope.Cabin,// 舱位
            PickUpPerson: {},
            DropOffPerson: {},
            AccompanyPerson: [],
            SpecialPassengerDetail:""
        };
        //身份证号码错误
        if (laGlobalLocalService.CheckStringIsEmpty($scope.passengename)) {
            bootbox.alert('请输入乘机人姓名！');
            return;
        }
        if (laGlobalLocalService.CheckStringIsEmpty($scope.startCity)) {
            bootbox.alert('请输入出发城市！');
            return;
        }
        if (laGlobalLocalService.CheckStringIsEmpty($scope.endCity)) {
            bootbox.alert('请输入到达城市！');
            return;
        }
        if (laGlobalLocalService.CheckStringIsEmpty($scope.flightNo)) {
            bootbox.alert('请输入航班号！');
            return;
        }
        if (laGlobalLocalService.CheckStringIsEmpty($scope.flightData)) {
            bootbox.alert('请输入乘机日期！');
            return;
        }
        if($("#customFildType").val()==1) {
            if (!laGlobalLocalService.IdentityCodeValid($scope.passengeFildId)) {
                bootbox.alert('乘机人的证件号码有误！请核对后再输入');
                return;
            }
        }
        if ($("#customAge").val()==""||$("#customAge").val()==undefined) {
            bootbox.alert('请输入乘机人的年龄');
            return;
        }
        var PickUpPerson = {
            "Name": "",
            "Relation": "",
            "Phone": "",
            "Email": ""
        };

            PickUpPerson.Name=$("#PickUpPersonName").val();
            PickUpPerson.Relation=$("#PickUpPersonRela").val();
            PickUpPerson.Phone=$("#PickUpPersonPhone").val();
            PickUpPerson.Email=$("#PickUpPersonEmail").val();
            if (PickUpPerson.Name==""||PickUpPerson.Name==undefined) {
                bootbox.alert('请填写送机人的姓名');
                return;
            }
            if (PickUpPerson.Relation==""||PickUpPerson.Relation==undefined) {
                bootbox.alert('请填写送机人和乘机人之间的关系');
                return;
            }

            if (PickUpPerson.Phone==""||PickUpPerson.Phone==undefined) {
                bootbox.alert('请填写送机人的联系电话');
                return;
            }
            if (PickUpPerson.Email==""||PickUpPerson.Email==undefined) {
                bootbox.alert('请填写送机人的电子邮箱');
                return;
            }
            if (!laGlobalLocalService.CheckEMailFormat(PickUpPerson.Email)) {
                bootbox.alert("请输入正确的送机人邮箱地址");
                return;
            }

        postdata.PickUpPerson=PickUpPerson;


        var DropOffPerson = {
            "Name": "",
            "Relation": "",
            "Phone": "",
            "Email": ""
        };

            DropOffPerson.Name=$("#DropOffPersonName").val();
            DropOffPerson.Relation=$("#DropOffPersonRela").val();
            DropOffPerson.Phone=$("#DropOffPersonPhone").val();
            DropOffPerson.Email=$("#DropOffPersonEmail").val();
            if (DropOffPerson.Name==""||DropOffPerson.Name==undefined) {
                bootbox.alert('请填写接机人的姓名');
                return;
            }
            if (DropOffPerson.Relation==""||DropOffPerson.Relation==undefined) {
                bootbox.alert('请填写接机人和乘机人之间的关系');
                return;
            }

            if (DropOffPerson.Phone==""||DropOffPerson.Phone==undefined) {
                bootbox.alert('请填写接机人的联系电话');
                return;
            }
            if (DropOffPerson.Email==""||DropOffPerson.Email==undefined) {
                bootbox.alert('请填写接机人的电子邮箱');
                return;
            }
            if (!laGlobalLocalService.CheckEMailFormat(DropOffPerson.Email)) {
                bootbox.alert("请输入正确的接机人邮箱地址");
                return;
            }

        postdata.DropOffPerson=DropOffPerson;
        if ($("#signature").val()==""||$("#signature").val()==undefined) {
            bootbox.alert('请申请人签名');
            return;
        }

        laUserService.ApplySpecialService(postdata, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == '0000') {
                $window.location.href = "/Others/Custom/SpecialServicesubmit.html?param=" + new Base64().encode(JSON.stringify(rs));
            }
            else {
                bootbox.alert(dataBack.Message);
            }
        })
    };
    /** 查询航班动态***/
    function QueryFlightDynamic() {
        var p = $scope.FlightInfo;
        laFlightService.QueryFlightDynamic(p.C1, p.C2, p.T, p.F, p.Q, function (dataBack, status) {
            $scope.FliDynamic = dataBack;
            if ($scope.FliDynamic.Code == laGlobalProperty.laServiceCode_Success && $scope.FliDynamic.DynamicsResultInfo.length > 0) {
                $scope.ArriveTime=$scope.FliDynamic.DynamicsResultInfo[0].expectArriveTime.substr(11,6);
                $scope.DepartureTime=$scope.FliDynamic.DynamicsResultInfo[0].expectDepartureTime.substr(11,6);
            } else {
                bootbox.alert("航班号有误或暂无此航班信息！");
                return;
            }
        });
    }
}]);

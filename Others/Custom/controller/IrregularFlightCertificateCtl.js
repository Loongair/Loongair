/**
 * Created by cy on 2016-12-22.
 */
laAir.controller('laAir_Custom_IrregularFlightCertificatePageCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "不正常航班证明下载";
    $document[0].title = $scope.title;

    $scope.Certificate;
    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.Certificate = JSON.parse(new Base64().decode(params[i].substr(6)));
                    }
                    catch (e) {

                    }
                    break;
                }
            }
        }
    }

    /**
     * 不正常航班证明文件发送到邮箱
     * @param fli
     * @param meal
     */
    $scope.btnIrregularFlightCertificate = function () {
        var applyInfo = {};
        var bdhtml = window.document.body.innerHTML;//获取当前页的html代码
        //document.getElementById('printcard').style.display = 'block';
        sprnstr = "<!--startprint0-->";//设置打印开始区域
        eprnstr = "<!--endprint0-->";//设置打印结束区域
        prnhtml = bdhtml.substring(bdhtml.indexOf(sprnstr) + 18); //从开始代码向后取html
        prnhtml = prnhtml.substring(0, prnhtml.indexOf(eprnstr));//从结束代码向前取html
        prnhtml = prnhtml.replace("style=\"display: none;\"", "style=\"display: block;\"");

        applyInfo.ToAddress=$scope.mailAddress;
        applyInfo.Doc=new Base64().encode(JSON.stringify(prnhtml));
        laUserService.SendIrregularFlightEMail(applyInfo, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == laGlobalProperty.laServiceCode_Success) {
                bootbox.alert(rs.Message);
            } else {
                bootbox.alert(rs.Message);
            }
        })
    };

    /**
     * 返回
     */
    $scope.btnBack = function () {
        $window.location.href = "/Others/Custom/SpecialMeal.html?param=" + new Base64().encode(JSON.stringify($scope.PassengerFlight));
    };
}]);
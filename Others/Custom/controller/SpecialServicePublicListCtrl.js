/**
 * Created by cy on 2017-03-07.
 */
laAir.controller('laAir_Others_SpecialServiceListCtrl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "特殊旅客订单列表";
    $document[0].title = $scope.title;
    $scope.isHomeNav = true;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;

}]);
/**
 * 特殊服务申请确认页面
 * Created by panShuang on 2017/3/15.
 */
laAir.controller('laAir_Custom_SpecialApplicationDetailPageCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "服务申请";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.PassengerName;
    $scope.PassengerFOIDNo;
    $scope.FlightNo;
    $scope.SpecialPassengerType;
    $scope.SpecialPassengerTypeCH;
    $scope.ApplyNo;
    $scope.ApplyDate;
    $scope.Remark;
    $scope.AirplaneType;
    $scope.ApplyStatue;
    $scope.ApplyStatueCH;
    $scope.FlightDate;
    $scope.DepartureTime1;
    $scope.ArriveTime1;
    $scope.DepartureTime2;
    $scope.ArriveTime2;
    $scope.DepartureCH;
    $scope.ArriveCH;
    $scope.Cabin;
    $scope.ApplySubmitList;
    $scope.CheckResult;
    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.ApplySubmitList = JSON.parse(new Base64().decode(params[i].substr(6)));
                    }
                    catch (e) {

                    }
                    break;
                }

            }
        }
    }
    if ($scope.ApplySubmitList != undefined && $scope.ApplySubmitList != null) {
        $scope.PassengerName = $scope.ApplySubmitList.PassengerName;
        $scope.PassengerFOIDNo = $scope.ApplySubmitList.PassengerFOIDNo;
        $scope.FlightNo = $scope.ApplySubmitList.FlightNo;
        $scope.SpecialPassengerType =$scope.ApplySubmitList.SpecialPassengerType;
        $scope.ApplyNo = $scope.ApplySubmitList.ApplyNo;
        $scope.ApplyDate = ($filter('date')($scope.ApplySubmitList.ApplyDate,'yyyy-MM-dd')).substring(0,10);
        $scope.Remark = $scope.ApplySubmitList.Remark;
        $scope.AirplaneType = $scope.ApplySubmitList.AirplaneType;
        $scope.ApplyStatue = $scope.ApplySubmitList.ApplyStatue;
        $scope.FlightDate = $scope.ApplySubmitList.FlightDate;
        $scope.DepartureTime1 = ($filter('date')($scope.ApplySubmitList.DepartureTime,'yyyy-MM-dd')).substring(0,10);
        $scope.ArriveTime1 = ($filter('date')($scope.ApplySubmitList.ArriveTime,'yyyy-MM-dd')).substring(0,10);
        $scope.DepartureTime2 = ($filter('date')($scope.ApplySubmitList.DepartureTime,'yyyy-MM-dd HH:mm:ss')).substring(11,19);
        $scope.ArriveTime2 = ($filter('date')($scope.ApplySubmitList.ArriveTime,'yyyy-MM-dd HH:mm:ss')).substring(11,19);
        $scope.DepartureCH = $scope.ApplySubmitList.DepartureCH;
        $scope.ArriveCH = $scope.ApplySubmitList.ArriveCH;
        $scope.Cabin = $scope.ApplySubmitList.Cabin;
        $scope.CheckResult=$scope.ApplySubmitList.Remark;
        switch ($scope.ApplyStatue)
        {
            case 1:
                $scope.ApplyStatueCH="已提交";
                break;
            case 2:
                $scope.ApplyStatueCH="已拒绝";
                bootbox.alert($scope.CheckResult);
                break;
            case 3:
                $scope.ApplyStatueCH="待审核";
                break;
            case 4:
                $scope.ApplyStatueCH="已取消";
                break;
        }
        switch ($scope.SpecialPassengerType)
        {
            case 1:
                $scope.SpecialPassengerTypeCH="轮椅旅客";
                break;
            case 2:
                $scope.SpecialPassengerTypeCH="无成人陪伴儿童旅客";
                break;
            case 3:
                $scope.SpecialPassengerTypeCH="孕妇旅客";
                break;
            case 4:
                $scope.SpecialPassengerTypeCH="病患旅客";
                break;
            case 5:
                $scope.SpecialPassengerTypeCH="婴儿";
                break;
            case 6:
                $scope.SpecialPassengerTypeCH="视力听觉障碍旅客";
                break;
        }
    }
    $scope.CancelSpecialService = function () {
        $window.location.href = "/Others/Custom/CancelApplication.html?param=" + new Base64().encode(JSON.stringify($scope.ApplySubmitList));
    }
}]);
/**
 * 特殊服务申请结果查询页面
 * Created by panShuang on 2017/3/15.
 */
laAir.controller('laAir_Custom_SpecialCheckDetailPageCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "服务详情";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.PassengerName;
    $scope.PassengerFOIDNo;
    $scope.FlightNo;
    $scope.SpecialPassengerType;
    $scope.SpecialPassengerTypeCH;
    $scope.ApplyNo;
    $scope.ApplyDate;
    $scope.Remark;
    $scope.AirplaneType;
    $scope.ApplyStatue;
    $scope.ApplyStatueCH;
    $scope.FlightDate;
    $scope.DepartureTime1;
    $scope.ArriveTime1;
    $scope.DepartureTime2;
    $scope.ArriveTime2;
    $scope.DepartureCH;
    $scope.ArriveCH;
    $scope.Cabin;
    $scope.ApplySubmitList;
    $scope.CheckResult;
    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.ApplySubmitList = JSON.parse(new Base64().decode(params[i].substr(6)));
                    }
                    catch (e) {

                    }
                    break;
                }

            }
        }
    }
    if ($scope.ApplySubmitList != undefined && $scope.ApplySubmitList != null) {
        $scope.PassengerName = $scope.ApplySubmitList.PassengerName;
        $scope.PassengerFOIDNo = $scope.ApplySubmitList.PassengerFOIDNo;
        $scope.FlightNo = $scope.ApplySubmitList.FlightNo;
        $scope.SpecialPassengerType =$scope.ApplySubmitList.SpecialPassengerType;
        $scope.ApplyNo = $scope.ApplySubmitList.ApplyNo;
        $scope.ApplyDate = ($filter('date')($scope.ApplySubmitList.ApplyDate,'yyyy-MM-dd')).substring(0,10);
        $scope.Remark = $scope.ApplySubmitList.Remark;
        $scope.AirplaneType = $scope.ApplySubmitList.AirplaneType;
        $scope.ApplyStatue = $scope.ApplySubmitList.ApplyStatue;
        $scope.FlightDate = $scope.ApplySubmitList.FlightDate;
        $scope.DepartureTime1 = ($filter('date')($scope.ApplySubmitList.DepartureTime,'yyyy-MM-dd')).substring(0,10);
        $scope.ArriveTime1 = ($filter('date')($scope.ApplySubmitList.ArriveTime,'yyyy-MM-dd')).substring(0,10);
        $scope.DepartureTime2 = ($filter('date')($scope.ApplySubmitList.DepartureTime,'yyyy-MM-dd HH:mm:ss')).substring(11,19);
        $scope.ArriveTime2 = ($filter('date')($scope.ApplySubmitList.ArriveTime,'yyyy-MM-dd HH:mm:ss')).substring(11,19);
        $scope.DepartureCH = $scope.ApplySubmitList.DepartureCH;
        $scope.ArriveCH = $scope.ApplySubmitList.ArriveCH;
        $scope.Cabin = $scope.ApplySubmitList.Cabin;
        $scope.CheckResult=$scope.ApplySubmitList.Remark;
        switch ($scope.ApplyStatue)
        {
            case 1:
                $scope.ApplyStatueCH="已通过";
                break;
            case 2:
                $scope.ApplyStatueCH="已拒绝";
                bootbox.alert($scope.CheckResult);
                break;
            case 3:
                $scope.ApplyStatueCH="待审核";
                break;
            case 4:
                $scope.ApplyStatueCH="已取消";
                break;
        }
        switch ($scope.SpecialPassengerType)
        {
            case 1:
                $scope.SpecialPassengerTypeCH="轮椅旅客";
                break;
            case 2:
                $scope.SpecialPassengerTypeCH="无成人陪伴儿童旅客";
                break;
            case 3:
                $scope.SpecialPassengerTypeCH="孕妇旅客";
                break;
            case 4:
                $scope.SpecialPassengerTypeCH="病患旅客";
                break;
            case 5:
                $scope.SpecialPassengerTypeCH="婴儿";
                break;
            case 6:
                $scope.SpecialPassengerTypeCH="视力听觉障碍旅客";
                break;
        }
    }
    $scope.CancelSpecialService = function () {
        $window.location.href = "/Others/Custom/CancelApplication.html?param=" + new Base64().encode(JSON.stringify($scope.ApplySubmitList));
    }
}]);
/**
 * Created by panShuang on 2017/3/14.
 */
laAir.controller('laAir_specialServicel_infoCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "特殊旅客介绍";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.SpecialPassengerType=1;
    $scope.ToApply=function () {
        $window.location.href = "/Others/Custom/SpecialWheel.html?param=" + new Base64().encode(JSON.stringify($scope.SpecialPassengerType));
    }

}]);
/**
 * Created by panShuang on 2017/3/14.
 */
laAir.controller('laAir_specialService_submitCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "提交申请";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
   /*
    public string PassengerName;
    public string PassengerFOIDNo;
    public string FlightNo;
    public SpecialPassengerType SpecialPassengerType;
    public string ApplyNo;
    public DateTime ApplyDate;
    public string Remark;
    public string AirplaneType;//机型
    public CheckStatue ApplyStatue;//申请状态
    public DateTime FlightDate;
    public string DepartureTime;// 计划起飞时间
    public string ArriveTime;// 计划到达时间
    public string DepartureCH;// 出发机场名称
    public string ArriveCH;// 到达机场中文
    public string Cabin;// 舱位
    */
    $scope.PassengerName;
    $scope.PassengerFOIDNo;
    $scope.PassengerFOIDType;
    $scope.FlightNo;
    $scope.SpecialPassengerType;
    $scope.ApplyNo;
    $scope.ApplyDate;
    $scope.ApplyDateCH;
    $scope.Remark;
    $scope.AirplaneType;
    $scope.ApplyStatue;
    $scope.ApplyStatueCH;
    $scope.FlightDate;
    $scope.DepartureTime;
    $scope.ArriveTime;
    $scope.DepartureCH;
    $scope.ArriveCH;
    $scope.Cabin;
    $scope.ApplySubmitList;
    $scope.CheckResult;
    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.ApplySubmitList = JSON.parse(new Base64().decode(params[i].substr(6)));
                    }
                    catch (e) {

                    }
                    break;
                }

            }
        }
    }
    if ($scope.ApplySubmitList != undefined && $scope.ApplySubmitList != null) {
        $scope.CheckResult=$scope.ApplySubmitList.Remark;
        if($scope.ApplySubmitList.ApplyStatue=="1"){
            $scope.ApplyStatueCH="已提交";
        }else  if($scope.ApplySubmitList.ApplyStatue=="2"){
            $scope.ApplyStatueCH="提交失败"
        }else  if($scope.ApplySubmitList.ApplyStatue=="3"){
            $scope.ApplyStatueCH="已申请，请勿重复提交申请";
            $(".special_custom_submit_box").css("width","700px");
        }if($scope.ApplySubmitList.ApplyStatue=="4"){
            $scope.ApplyStatueCH="已拒绝";
            bootbox.alert($scope.CheckResult);
        }

        var aSpecialPassengerType=["轮椅旅客","无成人陪伴儿童旅客","孕妇旅客","病患旅客","婴儿旅客","视力听力障碍旅客"];
        $scope.PassengerName = $scope.ApplySubmitList.PassengerName;
        $scope.PassengerFOIDNo = $scope.ApplySubmitList.PassengerFOIDNo;
        $scope.PassengerFOIDType = $scope.ApplySubmitList.PassengerFOIDType;
        $scope.FlightNo = $scope.ApplySubmitList.FlightNo;
        $scope.SpecialPassengerType = aSpecialPassengerType[$scope.ApplySubmitList.SpecialPassengerType-1];
        $scope.ApplyNo = $scope.ApplySubmitList.ApplyNo;
        $scope.ApplyDate = $scope.ApplySubmitList.ApplyDate;
        $scope.ApplyDateCH = ($filter('date')($scope.ApplySubmitList.ApplyDate,'yyyy-MM-dd')).substring(0,10)
        $scope.Remark = $scope.ApplySubmitList.Remark;
        $scope.AirplaneType = $scope.ApplySubmitList.AirplaneType;
        $scope.ApplyStatue = $scope.ApplySubmitList.ApplyStatue;
        $scope.FlightDate = $scope.ApplySubmitList.FlightDate;
        $scope.DepartureTime = $scope.ApplySubmitList.DepartureTime;
        $scope.ArriveTime = $scope.ApplySubmitList.ArriveTime;
        $scope.DepartureCH = $scope.ApplySubmitList.DepartureCH;
        $scope.ArriveCH = $scope.ApplySubmitList.ArriveCH;
        $scope.Cabin = $scope.ApplySubmitList.Cabin;
    }
    $scope.btnQueryInfoClick = function () {
        $window.location.href = "/Others/Custom/ApplicationDetail.html?param=" + new Base64().encode(JSON.stringify($scope.ApplySubmitList));
    }

}]);

/**取消特殊服务
 * Created by haoyang2587 on 2017/3/7.
 */
laAir.controller('laAir_Others_SpecialServiceCancelPageCtl', ['$document', '$interval', '$filter', '$scope', '$window', 'laFlightService', 'laUserService', 'laGlobalLocalService', function ($document, $interval, $filter, $scope, $window, laFlightService, laUserService, laGlobalLocalService) {
    $scope.title = "取消特殊服务";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.PassengerName;
    $scope.PassengerFOIDNo;
    $scope.FlightNo;
    $scope.SpecialPassengerType;
    $scope.SpecialPassengerTypeCH
    $scope.ApplyNo;
    $scope.ApplyDate;
    $scope.Remark;
    $scope.AirplaneType;
    $scope.ApplyStatue;
    $scope.ApplyStatueCH;
    $scope.FlightDate;
    $scope.DepartureTime1;
    $scope.ArriveTime1;
    $scope.DepartureTime2;
    $scope.ArriveTime2;
    $scope.DepartureCH;
    $scope.ArriveCH;
    $scope.Cabin;
    $scope.ApplySubmitList;
    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.ApplySubmitList = JSON.parse(new Base64().decode(params[i].substr(6)));
                    }
                    catch (e) {

                    }
                    break;
                }

            }
        }
    }
    if ($scope.ApplySubmitList != undefined && $scope.ApplySubmitList != null) {
        var aSpecialPassengerType=["轮椅旅客","无成人陪伴儿童旅客","孕妇旅客","病患旅客","婴儿","视觉听觉障碍旅客"];
        $scope.PassengerName = $scope.ApplySubmitList.PassengerName;
        $scope.PassengerFOIDNo = $scope.ApplySubmitList.PassengerFOIDNo;
        $scope.FlightNo = $scope.ApplySubmitList.FlightNo;
        $scope.SpecialPassengerType = $scope.ApplySubmitList.SpecialPassengerType;
        $scope.SpecialPassengerTypeCH = aSpecialPassengerType[$scope.ApplySubmitList.SpecialPassengerType-1];
        $scope.ApplyNo = $scope.ApplySubmitList.ApplyNo;
        $scope.ApplyDate = ($filter('date')($scope.ApplySubmitList.ApplyDate,'yyyy-MM-dd')).substring(0,10);
        $scope.Remark = $scope.ApplySubmitList.Remark;
        $scope.AirplaneType = $scope.ApplySubmitList.AirplaneType;
        $scope.ApplyStatue = $scope.ApplySubmitList.ApplyStatue;
        $scope.FlightDate = $scope.ApplySubmitList.FlightDate;
        $scope.DepartureTime1 = ($filter('date')($scope.ApplySubmitList.DepartureTime,'yyyy-MM-dd')).substring(0,10);
        $scope.ArriveTime1 = ($filter('date')($scope.ApplySubmitList.ArriveTime,'yyyy-MM-dd')).substring(0,10);
        $scope.DepartureTime2 = ($filter('date')($scope.ApplySubmitList.DepartureTime,'yyyy-MM-dd HH:mm:ss')).substring(11,19);
        $scope.ArriveTime2 = ($filter('date')($scope.ApplySubmitList.ArriveTime,'yyyy-MM-dd HH:mm:ss')).substring(11,19);
        $scope.DepartureCH = $scope.ApplySubmitList.DepartureCH;
        $scope.ArriveCH = $scope.ApplySubmitList.ArriveCH;
        $scope.Cabin = $scope.ApplySubmitList.Cabin;
        switch ($scope.ApplyStatue)
        {
            case 1:
                $scope.ApplyStatueCH="已通过";
                break;
            case 2:
                $scope.ApplyStatueCH="已拒绝";
                break;
            case 3:
                $scope.ApplyStatueCH="待审核";
                break;
            case 4:
                $scope.ApplyStatueCH="已取消";
                break;
        }
    }
    $scope.checkOk=function () {
        var postdata={
            ApplyNo:$scope.ApplyNo
        };
        laUserService.CancelSpecialService(postdata, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == '0000') {
                if(rs.ResultStr==1){
                    $('.modal').modal('show');
                }else if(rs.ResultStr==2){
                    bootbox.alert("取消申请失败！");
                }else if(rs.ResultStr==3){
                    bootbox.alert("未查到申请记录！");
                }else if(rs.ResultStr==4){
                    bootbox.alert("申请已被拒绝，不能取消！");
                }else if(rs.ResultStr==5){
                    bootbox.alert("已取消，不能再次取消！");
                }

            }
            else {
                bootbox.alert("操作失败！");
            }
        })


    };
    $scope.btnCheck=function (postdata) {
        laUserService.GetSpecialServiceByApplyNo(postdata, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == '0000') {
                $window.location.href = "/Others/Custom/SpecialServiceCheckDetail.html?param=" + new Base64().encode(JSON.stringify(rs));
            }
            else {
                bootbox.alert(dataBack.Message);
            }
        })
    }

}]);

/**
 * 查询特殊旅客特殊服务信息接口
 */
laAir.controller('laAir_Others_SpecialServiceSearchCtrl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "申请单查询";
    $document[0].title = $scope.title;
    $scope.isHomeNav = true;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.btnQueryTicketClick = function () {
        var SpecialPassengerType = $("#SpecialPassengerType").val();
        var PassengerName = $("#CustomName").val();
        var flightNo = $("#FlightNo").val();
        var FlightDate = $("#PlaneTime").val();
        var FOIDType = $("#FOIDType").val();
        var FOIDNo = $("#FOIDNo").val() || $("#FOIDNo2").val();
        var QueryTag = $("#r1").val();
        var ApplyNo = $("#ApplyNo").val();

        $scope.qu_customName = PassengerName;
        $scope.qu_planeTime = FlightDate;
        $scope.qu_flightNo = flightNo;
        $scope.qu_PassengerType = SpecialPassengerType;
        $scope.qu_Cert = FOIDType;
        $scope.qu_CertNo = FOIDNo;
        $scope.qu_ApplyNo = ApplyNo;


        if (QueryTag == 1) {
            //姓名为空
            if (laGlobalLocalService.CheckStringIsEmpty($scope.qu_customName)) {
                bootbox.alert('请输入姓名');
                return;
            }
            //航班时间为空
            if (laGlobalLocalService.CheckStringIsEmpty($scope.qu_planeTime)) {
                bootbox.alert('请选择航班时间');
                return;
            }
            //航班号为空
            if (laGlobalLocalService.CheckStringIsEmpty($scope.qu_flightNo)) {
                bootbox.alert('请输入航班号');
                return;
            }
            //手机号为空
            if (laGlobalLocalService.CheckStringIsEmpty($scope.qu_PassengerType)) {
                bootbox.alert('请选择服务类型');
                return;
            }
            //证件类型为空
            if (laGlobalLocalService.CheckStringIsEmpty($scope.qu_Cert)) {
                bootbox.alert('请选择证件类型');
                return;
            }
            //证件号码为空
            if (laGlobalLocalService.CheckStringIsEmpty($scope.qu_CertNo)) {
                bootbox.alert('请输入证件号码');
                return;
            }
            //身份证号码错误
            if ($("#FOIDType").val() == 1) {
                if (!laGlobalLocalService.IdentityCodeValid($scope.qu_CertNo)) {
                    bootbox.alert('您的证件号码有误！请核对后再输入');
                    return;
                }
            }

        } else {
            if (laGlobalLocalService.CheckStringIsEmpty($scope.qu_ApplyNo)) {
                bootbox.alert('请输入申请单号');
                return;
            }
        }

        var postdata = {
            "QueryTag": "1",
            "PassengerName": "",
            "FlightDate": "",
            "FlightNo": "",
            "SpecialPassengerType": "",
            "PassengerFOIDType": 1,
            "PassengerFOIDNo": "",
            "ApplyNo": ""
        };
        postdata.PassengerName = $("#CustomName").val();
        postdata.FlightDate = $("#PlaneTime").val();
        postdata.FlightNo = $("#FlightNo").val();
        postdata.SpecialPassengerType = $("#SpecialPassengerType").val();
        postdata.PassengerFOIDType = $("#FOIDType").val();
        postdata.PassengerFOIDNo=$("#FOIDNo").val() || $("#FOIDNo2").val();
        postdata.QueryTag = $("#r1").val();
        postdata.ApplyNo = $("#ApplyNo").val();
        laUserService.CheckSpecialServiceByApplyNo(postdata, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == laGlobalProperty.laServiceCode_Success) {
                var results= rs.SpecialServiceInfo;
                $window.location.href = "/Others/Custom/SpecialServiceStatuList.html?param=" + new Base64().encode(JSON.stringify(results));
            } else {
                bootbox.alert(dataBack.Message);
            }
        })
    };

}]);

/**
 * 特殊旅客服务进度查询
 */
laAir.controller('laAir_specialServicel_statu_ListCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "特殊旅客服务进度查询";
    $document[0].title = $scope.title;
    $scope.menuListFood = laMapMenu_SpecialFood;
    $scope.menuListPassenger = laMapMenu_SpecialPassenger;
    $scope.menuListPrepaidOptions = laMapMenu_PrepaidOptions;
    $scope.menuListPrepaidBaggage = laMapMenu_PrepaidBaggage;
    $scope.menuListPsgPrepaidMailItinerary = laMapMenu_MailItinerary;
    $scope.ApplyStatuelist;
    $scope.PassengerFOIDTypeCH=[];
    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.ApplyStatuelist = JSON.parse(new Base64().decode(params[i].substr(6)));
                    }
                    catch (e) {

                    }
                    break;
                }

            }
        }
    }
    if ($scope.ApplyStatuelist != undefined && $scope.ApplyStatuelist != null) {
        for(i=0;i<$scope.ApplyStatuelist.length;i++){
            switch ($scope.ApplyStatuelist[i].SpecialPassengerType)
            {
                case 1:
                    $scope.ApplyStatuelist[i].SpecialPassengerType="轮椅旅客";
                    break;
                case 2:
                    $scope.ApplyStatuelist[i].SpecialPassengerType="无成人陪伴儿童旅客";
                    break;
                case 3:
                    $scope.ApplyStatuelist[i].SpecialPassengerType="孕妇旅客";
                    break;
                case 4:
                    $scope.ApplyStatuelist[i].SpecialPassengerType="病患旅客";
                    break;
                case 5:
                    $scope.ApplyStatuelist[i].SpecialPassengerType="婴儿";
                    break;
                case 6:
                    $scope.ApplyStatuelist[i].SpecialPassengerType="视力听觉障碍旅客";
                    break;
            }
            switch ($scope.ApplyStatuelist[i].ApplyStatue)
            {
                case 1:
                    $scope.ApplyStatuelist[i].ApplyStatue="已通过";
                    break;
                case 2:
                    $scope.ApplyStatuelist[i].ApplyStatue="已拒绝";
                    break;
                case 3:
                    $scope.ApplyStatuelist[i].ApplyStatue="待审核";
                    break;
                case 4:
                    $scope.ApplyStatuelist[i].ApplyStatue="已取消";
                    break;
            }
            $scope.ApplyStatuelist[i].FlightDate=($filter('date')($scope.ApplyStatuelist[i].FlightDate,'yyyy-MM-dd')).substring(0,10);
            switch ($scope.ApplyStatuelist[i].PassengerFOIDType)
            {
                case 1:
                    $scope.PassengerFOIDTypeCH[i]="身份证";
                    break;
                case 2:
                    $scope.PassengerFOIDTypeCH[i]="护照";
                    break;
                case 3:
                    $scope.PassengerFOIDTypeCH[i]="台胞证";
                    break;
                case 4:
                    $scope.PassengerFOIDTypeCH[i]="港澳通行证";
                    break;
                case 100:
                    $scope.PassengerFOIDTypeCH[i]="其他证件";
                    break;
            }

        }
    }
    $scope.btnCheck=function (postdata) {
        laUserService.GetSpecialServiceByApplyNo(postdata.ApplyNo, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == '0000') {
                $window.location.href = "/Others/Custom/SpecialServiceCheckDetail.html?param=" + new Base64().encode(JSON.stringify(rs));
            }
            else {
                bootbox.alert(dataBack.Message);
            }
        })
    }

}]);

/**
 * Created by panShuang on 2017/3/14.
 * 根据票号、姓名、手机号查询特殊服务结果
 */
laAir.controller('laAir_Custom_SpecialWheelPageCtl', ['$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($interval, $document, $window, $scope, laUserService, laGlobalLocalService) {

    $scope.title = "特殊旅客申请查询";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

    $scope.tic1 = "891";
    $scope.tic2 = "";
    $scope.TicketNum = "";
    $scope.PassengerName;
    $scope.Mobile;
    $scope.isCommitQuery = false;
    $scope.Param;
    $scope.SpecialPassengerType;

    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.Param = JSON.parse(new Base64().decode(params[i].substr(6)));
                        $scope.TicketNum = $scope.Param.TktNo;
                        $scope.tic1 = $scope.TicketNum.split('-')[0];
                        $scope.tic2 = $scope.TicketNum.split('-')[1];
                        $scope.PassengerName = $scope.Param.Name;
                        $scope.Mobile = $scope.Param.Mobile;
                        $scope.SpecialPassengerType=$scope.Param;
                    }
                    catch (e) {

                    }
                    break;
                }

            }
        }
    }

    $scope.btnTicPartChange = function () {
        $scope.TicketNum = $scope.tic1 + "-" + $scope.tic2;
        if ($scope.tic1.length == 3) {
            $("#tic2").focus();
        }
    };

    $scope.btnQuerySpecialMeal = function () {
        if (laGlobalLocalService.CheckStringIsEmpty($scope.TicketNum)) {
            bootbox.alert('请输入电子票号');
            return;
        }
        if (!laGlobalLocalService.CheckStringLength($scope.TicketNum, 14)) {
            bootbox.alert('请输入13位电子票号');
            return;
        }

        var ticFormatDesc = "请输入格式正确的电子票号";
        var tic = $scope.TicketNum.split('-');
        if (tic.length != 2) {
            bootbox.alert(ticFormatDesc);
            return;
        }
        for (var i = 0; i < tic.length; i++) {
            if (!laGlobalLocalService.IsNum(tic[i])) {
                bootbox.alert(ticFormatDesc);
                return;
            }
        }

        if (laGlobalLocalService.CheckStringIsEmpty($scope.PassengerName)) {
            bootbox.alert('请输入旅客姓名');
            return;
        }
        if (laGlobalLocalService.CheckStringIsEmpty($scope.Mobile) || !laGlobalLocalService.CheckStringLength($scope.Mobile, 11) || !laGlobalLocalService.CheckMobileCode($scope.Mobile)) {
            bootbox.alert("请输入11位正确的手机号码");
            return;
        }

        $scope.isCommitQuery = true;
        var queryInfo = {"TktNo": $scope.TicketNum, "Name": $scope.PassengerName, "Mobile": $scope.Mobile,"SpecialPassengerType":$scope.Param};
        $window.location.href = "/Others/Custom/SpecialServiceLineList.html?param=" + new Base64().encode(JSON.stringify(queryInfo));
    };
}]);
/**
 * 特殊旅客特殊服务航班列表
 *
 */
laAir.controller('laAir_Custom_SpecialServiceLineListPageCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "预订特殊服务";
    $document[0].title = $scope.title;

    $scope.Param;
    $scope.WheelList;
    $scope.QueryResultDesc;
    $scope.SpecialPassengerType;

    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.Param = JSON.parse(new Base64().decode(params[i].substr(6)));
                    }
                    catch (e) {

                    }
                    break;
                }

            }
        }
    }

    if ($scope.Param != undefined && $scope.Param != null) {
        $scope.SpecialPassengerType=$scope.Param.SpecialPassengerType;
        QuerySpecialWheelPidDetrTN();
    }

    /**
     * 预订特殊服务
     * @param fli
     * @param meal
     */
    $scope.btnApply = function (fli) {
        if(fli.Departure!="HGH"){
            bootbox.confirm("对不起，现在只支持杭州出发旅客申请该服务！");
            return false;
        }

        bootbox.confirm("您确定要预订该服务吗?", function (result) {
            if (result) {
                var applyInfo = {};
                applyInfo.DepartureTime = $filter('date')(fli.DepartureTime, 'yyyy-MM-dd HH:mm:ss');
                applyInfo.ArriveTime = $filter('date')(fli.ArriveTime, 'yyyy-MM-dd HH:mm:ss');
                applyInfo.Departure = fli.Departure;
                applyInfo.Arrive = fli.Arrive;
                applyInfo.PassengerName = $scope.Param.Name;
                applyInfo.PassengerId = "";
                applyInfo.FlightNo = fli.FlightNo;
                applyInfo.FlightData=$filter('date')(fli.FlightDate,'yyyy-MM-dd');
                applyInfo.DepartureCityCH = fli.DepartureCityCH;
                applyInfo.ArriveCityCH=fli.ArriveCityCH;
                applyInfo.TicketNo = $scope.Param.TktNo;
                applyInfo.Mobile = $scope.Param.Mobile;
                applyInfo.AirplaneType = fli.AirplaneType;
                applyInfo.DepartureCH=fli.DepartureCH;
                applyInfo.ArriveCH=fli.ArriveCH;
                applyInfo.Cabin = fli.Cabin;
                applyInfo.TicketStatue=fli.TicketStatue;
                switch ($scope.SpecialPassengerType)
                {
                    case 1:
                        $window.location.href = "/Others/Custom/SpecialServiceWheelApply.html?param=" + new Base64().encode(JSON.stringify(applyInfo));
                        break;
                    case 2:
                        $window.location.href = "/Others/Custom/SpecialServiceChildApply.html?param=" + new Base64().encode(JSON.stringify(applyInfo));
                        break;
                    case 3:
                        $window.location.href = "/Others/Custom/SpecialServicePregnantApply.html?param=" + new Base64().encode(JSON.stringify(applyInfo));
                        break;
                    case 4:
                        $window.location.href = "/Others/Custom/SpecialServicePatientApply.html?param=" + new Base64().encode(JSON.stringify(applyInfo));
                        break;
                    case 5:
                        $window.location.href = "/Others/Custom/SpecialServiceBabyApply.html?param=" + new Base64().encode(JSON.stringify(applyInfo));
                        break;
                    case 6:
                        $window.location.href = "/Others/Custom/SpecialServiceVision_hearingApply.html?param=" + new Base64().encode(JSON.stringify(applyInfo));
                        break;
                }

            }
        });
    };

    /**
     * 返回
     */
    $scope.btnBack = function () {
        $window.location.href = "/Others/Custom/SpecialWheel.html?param=" + new Base64().encode(JSON.stringify($scope.Param));
    };

    /**
     * 查询
     */
    function QuerySpecialWheelPidDetrTN() {
        laUserService.CheckSpecialService($scope.Param, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == laGlobalProperty.laServiceCode_Success) {
                $scope.WheelList = rs;
            } else {
                $scope.QueryResultDesc = rs.Message;
            }
        })
    }
}]);

/**
 * Created by Jerry on 16/2/23.
 */

laAir.controller('laAir_Custom_ETicketVerifyPageCtl', ['$interval', '$document', '$scope', 'laOrderService', 'laGlobalLocalService', function ($interval, $document, $scope, laOrderService, laGlobalLocalService) {

    $scope.title = "机票验真";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

    $scope.tic1 = "891";
    $scope.tic2 = "";
    $scope.TicketNum = "";
    $scope.PassengerName;
    $scope.ImgVerifyCode;
    $scope.ValidCode = "";
    $scope.sessionId;
    $scope.isCommitQuery = false;
    $scope.CheckedDesc = "";

    GetImgVerifyCode();

    $scope.btnChangeVerifyCode = function () {
        GetImgVerifyCode();
    };

    $scope.btnTicPartChange = function () {
        $scope.TicketNum = $scope.tic1 + "-" + $scope.tic2;
        if ($scope.tic1.length == 3) {
            $("#tic2").focus();
        }
    };

    $scope.btnCheckTicket = function () {
        if (laGlobalLocalService.CheckStringIsEmpty($scope.TicketNum)) {
            bootbox.alert('请输入电子票号');
            return;
        }
        if (!laGlobalLocalService.CheckStringLength($scope.TicketNum, 14)) {
            bootbox.alert('请输入13位电子票号');
            return;
        }

        var ticFormatDesc = "请输入格式正确的电子票号";
        var tic = $scope.TicketNum.split('-');
        if (tic.length != 2) {
            bootbox.alert(ticFormatDesc);
            return;
        }
        for (var i = 0; i < tic.length; i++) {
            if (!laGlobalLocalService.IsNum(tic[i])) {
                bootbox.alert(ticFormatDesc);
                return;
            }
        }

        if (laGlobalLocalService.CheckStringIsEmpty($scope.PassengerName)) {
            bootbox.alert('请输入旅客姓名');
            return;
        }
        if (laGlobalLocalService.CheckStringIsEmpty($scope.ValidCode)) {
            bootbox.alert('请输入图片验证码');
            return;
        }

        $scope.isCommitQuery = true;
        $scope.CheckedDesc = "";
        laOrderService.CheckTicket($scope.TicketNum, $scope.PassengerName, $scope.ValidCode, $scope.sessionId, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == laGlobalProperty.laServiceCode_Success) {
                $scope.timeDown = 1;
                timer = $interval(function () {
                    $scope.timeDown = $scope.timeDown - 1;
                    if ($scope.timeDown <= 0) {
                        $interval.cancel(timer);

                        $scope.ValidCode = "";
                        GetImgVerifyCode();

                        $scope.isCommitQuery = false;
                        $scope.CheckedDesc = "经查验，存在客票号 [" + $scope.TicketNum + "] 的乘机人 [" + $scope.PassengerName + "]。感谢您选择长龙航空。";
                    }
                }, 1000);
            } else {
                $scope.timeDown = 2;
                timer = $interval(function () {
                    $scope.timeDown = $scope.timeDown - 1;
                    if ($scope.timeDown <= 0) {
                        $interval.cancel(timer);

                        $scope.ValidCode = "";
                        GetImgVerifyCode();

                        $scope.isCommitQuery = false;
                        //$scope.CheckedDesc = "对不起，经查验客票号 [" + $scope.TicketNum + "] 的乘机人 [" + $scope.PassengerName + "] 不存在," + rs.Message + "。请核对信息是否正确输入。";
                        bootbox.alert("验证失败:" + rs.Message);
                    }
                }, 1000);
            }
        })
    };

    function GetImgVerifyCode() {
        $scope.ImgVerifyCode = '';
        laOrderService.ImageVerifyCodeForCheckTicket(function (backData, status) {
            var rs = backData;
            if (rs.Code == laGlobalProperty.laServiceCode_Success) {
                $scope.ImgVerifyCode = backData.ImageVerifyCode;
                $scope.sessionId = rs.SessionID;
            }
        });
    }

}]);

laAir.controller('laAir_Custom_ProblemPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "常见问题";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

laAir.controller('laAir_Custom_RefundRulePageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "退改签规定";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);
laAir.controller('laAir_Custom_InvoluntaryRefundPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "非自愿退票说明";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

laAir.controller('laAir_Custom_RefundApplyPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "申请退票";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

laAir.controller('laAir_Custom_SpecialMealPageCtl', ['$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($interval, $document, $window, $scope, laUserService, laGlobalLocalService) {

    $scope.title = "预订餐食";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

    $scope.tic1 = "891";
    $scope.tic2 = "";
    $scope.TicketNum = "";
    $scope.PassengerName;
    $scope.Mobile;
    $scope.isCommitQuery = false;
    $scope.Param;

    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.Param = JSON.parse(new Base64().decode(params[i].substr(6)));
                        $scope.TicketNum = $scope.Param.TktNo;
                        $scope.tic1 = $scope.TicketNum.split('-')[0];
                        $scope.tic2 = $scope.TicketNum.split('-')[1];
                        $scope.PassengerName = $scope.Param.Name;
                        $scope.Mobile = $scope.Param.Mobile;
                    }
                    catch (e) {

                    }
                    break;
                }

            }
        }
    }

    $scope.btnTicPartChange = function () {
        $scope.TicketNum = $scope.tic1 + "-" + $scope.tic2;
        if ($scope.tic1.length == 3) {
            $("#tic2").focus();
        }
    };

    $scope.btnQuerySpecialMeal = function () {
        if (laGlobalLocalService.CheckStringIsEmpty($scope.TicketNum)) {
            bootbox.alert('请输入电子票号');
            return;
        }
        if (!laGlobalLocalService.CheckStringLength($scope.TicketNum, 14)) {
            bootbox.alert('请输入13位电子票号');
            return;
        }

        var ticFormatDesc = "请输入格式正确的电子票号";
        var tic = $scope.TicketNum.split('-');
        if (tic.length != 2) {
            bootbox.alert(ticFormatDesc);
            return;
        }
        for (var i = 0; i < tic.length; i++) {
            if (!laGlobalLocalService.IsNum(tic[i])) {
                bootbox.alert(ticFormatDesc);
                return;
            }
        }

        if (laGlobalLocalService.CheckStringIsEmpty($scope.PassengerName)) {
            bootbox.alert('请输入旅客姓名');
            return;
        }
        if (laGlobalLocalService.CheckStringIsEmpty($scope.Mobile) || !laGlobalLocalService.CheckStringLength($scope.Mobile, 11) || !laGlobalLocalService.CheckMobileCode($scope.Mobile)) {
            bootbox.alert("请输入11位正确的手机号码");
            return;
        }

        $scope.isCommitQuery = true;
        var queryInfo = {"TktNo": $scope.TicketNum, "Name": $scope.PassengerName, "Mobile": $scope.Mobile};
        $window.location.href = "/Others/Custom/SpecialMealList.html?param=" + new Base64().encode(JSON.stringify(queryInfo));
    };
}]);

laAir.controller('laAir_Custom_SpecialMealListPageCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "预订餐食";
    $document[0].title = $scope.title;

    $scope.Param;
    $scope.MealList;
    $scope.QueryResultDesc;

    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.Param = JSON.parse(new Base64().decode(params[i].substr(6)));
                    }
                    catch (e) {

                    }
                    break;
                }

            }
        }
    }

    if ($scope.Param != undefined && $scope.Param != null) {
        QuerySpecialMealPidDetrTN();
    }

    /**
     * 预订餐食
     * @param fli
     * @param meal
     */
    $scope.btnApply = function (fli, meal) {

        bootbox.confirm("您确定要预订该餐食吗?", function (result) {
            if (result) {
                var applyInfo = {};
                applyInfo.DepartureTime = $filter('date')(fli.DepartureTime, 'yyyy-MM-dd HH:mm:ss');
                applyInfo.ArriveTime = $filter('date')(fli.ArriveTime, 'yyyy-MM-dd HH:mm:ss');
                applyInfo.Departure = fli.Departure;
                applyInfo.Arrive = fli.Arrive;
                applyInfo.PassengerName = $scope.Param.Name;
                applyInfo.PassengerId = "";
                applyInfo.ApplyMeal = meal.MealCode;
                applyInfo.FlightNo = fli.FlightNo;
                applyInfo.TicketNo = $scope.Param.TktNo;
                applyInfo.Mobile = $scope.Param.Mobile;

                laUserService.ApplySpecialMeal(applyInfo, function (dataBack, status) {
                    var rs = dataBack;
                    if (rs.Code == laGlobalProperty.laServiceCode_Success) {
                        bootbox.alert("餐食预订成功");
                        QuerySpecialMealPidDetrTN();
                    } else {
                        bootbox.alert(rs.Message);
                    }
                })
            }
        });
    };

    /**
     * 取消餐食
     * @param fli
     * @param meal
     */
    $scope.btnCancel = function (fli, meal) {

        bootbox.confirm("您确定要取消该餐食吗?", function (result) {
            if (result) {
                var cancelInfo = {};
                cancelInfo.DepartureTime = $filter('date')(fli.DepartureTime, 'yyyy-MM-dd HH:mm:ss');
                cancelInfo.ArriveTime = $filter('date')(fli.ArriveTime, 'yyyy-MM-dd HH:mm:ss');
                cancelInfo.Departure = fli.Departure;
                cancelInfo.Arrive = fli.Arrive;
                cancelInfo.PassengerName = $scope.Param.Name;
                cancelInfo.PassengerId = "";
                cancelInfo.ApplyMeal = meal.MealCode;
                cancelInfo.FlightNo = fli.FlightNo;
                cancelInfo.TicketNo = $scope.Param.TktNo;

                laUserService.CancelSpecialMeal(cancelInfo, function (dataBack, status) {
                    var rs = dataBack;
                    if (rs.Code == laGlobalProperty.laServiceCode_Success) {
                        QuerySpecialMealPidDetrTN();
                        bootbox.alert("餐食取消成功");
                    } else {
                        bootbox.alert(rs.Message);
                    }
                })
            }
        });
    };

    /**
     * 返回
     */
    $scope.btnBack = function () {
        $window.location.href = "/Others/Custom/SpecialMeal.html?param=" + new Base64().encode(JSON.stringify($scope.Param));
    };

     /**
     * 查询
     */
    function QuerySpecialMealPidDetrTN() {
        laUserService.QuerySpecialMealPidDetrTN($scope.Param, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == laGlobalProperty.laServiceCode_Success) {
                $scope.MealList = rs;
            } else {
                $scope.QueryResultDesc = rs.Message;
            }
        })
    }
}]);

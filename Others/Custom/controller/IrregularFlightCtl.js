/**
 * Created by cy on 2016-12-21.
 */

laAir.controller('laAir_Custom_IrregularFlightPageCtl', ['$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($interval, $document, $window, $scope, laUserService, laGlobalLocalService) {

    $scope.title = "不正常航班证明查询";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

    $scope.tic1 = "891";
    $scope.tic2 = "";
    $scope.TicketNum = "";
    $scope.PassengerName;
    $scope.Foid;
    $scope.FlightData=today;
    $scope.isCommitQuery = false;
    $scope.Param;
    var today = new Date();
    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.Param = JSON.parse(new Base64().decode(params[i].substr(6)));
                        $scope.TicketNum = $scope.Param.TktNo;
                        $scope.tic1 = $scope.TicketNum.split('-')[0];
                        $scope.tic2 = $scope.TicketNum.split('-')[1];
                        $scope.PassengerName = $scope.Param.Name;
                        $scope.Foid = $scope.Param.Foid;
                    }
                    catch (e) {

                    }
                    break;
                }

            }
        }
    }

    $scope.btnTicPartChange = function () {
        $scope.TicketNum = $scope.tic1 + "-" + $scope.tic2;
        if ($scope.tic1.length == 3) {
            $("#tic2").focus();
        }
    };

    $scope.btnQueryIrregularFlight = function () {
        if (laGlobalLocalService.CheckStringIsEmpty($scope.TicketNum)) {
            bootbox.alert('请输入电子票号');
            return;
        }
        if (!laGlobalLocalService.CheckStringLength($scope.TicketNum, 14)) {
            bootbox.alert('请输入13位电子票号');
            return;
        }

        var ticFormatDesc = "请输入格式正确的电子票号";
        var tic = $scope.TicketNum.split('-');
        if (tic.length != 2) {
            bootbox.alert(ticFormatDesc);
            return;
        }
        for (var i = 0; i < tic.length; i++) {
            if (!laGlobalLocalService.IsNum(tic[i])) {
                bootbox.alert(ticFormatDesc);
                return;
            }
        }

        if (laGlobalLocalService.CheckStringIsEmpty($scope.PassengerName)) {
            bootbox.alert('请输入旅客姓名');
            return;
        }
        if (laGlobalLocalService.CheckStringIsEmpty($scope.Foid)) {
            bootbox.alert("请输入正确的证件号码");
            return;
        }
        if (!laGlobalLocalService.CheckStringIsEmpty($scope.FlightData)) {
            bootbox.alert('请输入正确的航班时间');
            return;
        }

        $scope.isCommitQuery = true;
        var queryInfo = {"TktNo": $scope.TicketNum, "Name": $scope.PassengerName, "Foid": $scope.Foid,"FlightData": $scope.FlightData};
        laUserService.QueryIrregularFlight(queryInfo, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == laGlobalProperty.laServiceCode_Success) {
                $scope.MealList = rs;
                rs.TktNo=$scope.TicketNum;
                rs.Foid=$scope.Foid;
                $window.location.href = "/Others/Custom/IrregularFlightList.html?param=" + new Base64().encode(JSON.stringify(rs));
            } else {
                bootbox.alert(rs.Message);
                $scope.isCommitQuery=false;
            }
        })
    };
}]);

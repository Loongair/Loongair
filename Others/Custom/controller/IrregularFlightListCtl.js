/**
 * Created by cy on 2016-12-22.
 */
laAir.controller('laAir_Custom_IrregularFlightListPageCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "航班选择";
    $document[0].title = $scope.title;

    $scope.PassengerFlight;
    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.PassengerFlight = JSON.parse(new Base64().decode(params[i].substr(6)));
                    }
                    catch (e) {

                    }
                    break;
                }
            }
        }
    }

    /**
     * 不正常航班下载
     * @param fli
     * @param meal
     */
    $scope.btnQueryIrregularFlightDetail = function (fli, meal) {
        var applyInfo = {};
        applyInfo.FlightDate = $filter('date')(fli.DepartureTime, 'yyyy-MM-dd HH:mm:ss');
        //applyInfo.ArriveTime = $filter('date')(fli.ArriveTime, 'yyyy-MM-dd HH:mm:ss');
        applyInfo.Departure = fli.Departure;
        applyInfo.Arrive = fli.Arrive;
        applyInfo.Name = $scope.PassengerFlight.PassengerName;
        applyInfo.IdNo = $scope.PassengerFlight.Foid;
        applyInfo.FlightNo = fli.FlightNo;
        applyInfo.TicketNo = $scope.PassengerFlight.TktNo;

        laUserService.QueryIrregularFlightDetail(applyInfo, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == laGlobalProperty.laServiceCode_Success) {
                $window.location.href = "/Others/Custom/IrregularFlightCertificate.html?param=" + new Base64().encode(JSON.stringify(rs));
            } else {
                bootbox.alert(rs.Message);
            }
        })
    };

    /**
     * 返回
     */
    $scope.btnBack = function () {
        $window.location.href = "/Others/Custom/SpecialMeal.html?param=" + new Base64().encode(JSON.stringify($scope.PassengerFlight));
    };
}]);
/**
 * Created by Jerry on 16/2/23.
 */

laAir.controller('laAir_MoreInfo_PartnerPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "合作伙伴";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

laAir.controller('laAir_MoreInfo_LegalStatementPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "法律声明";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

laAir.controller('laAir_MoreInfo_PrivacyPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "隐私保护";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

laAir.controller('laAir_MoreInfo_TransportPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "运输条款";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

laAir.controller('laAir_MoreInfo_PurchaseAgreementPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "购票协议";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

laAir.controller('laAir_MoreInfo_InsurancePageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "长龙航网站保险业务合作方资质证明材料及保险产品、服务信息披露内容";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

laAir.controller('laAir_MoreInfo_DelayInsPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "长龙航网站保险业务合作方资质证明材料及保险产品、服务信息披露内容";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

laAir.controller('laAir_MoreInfo_ClubPrivacyPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "俱乐部规则与条款";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

laAir.controller('laAir_MoreInfo_TipPageCtl', ['$document', '$scope', '$sce', '$window', 'laUserService', function ($document, $scope, $sce, $window, laUserService) {

    $scope.title = "温馨提示";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

    $scope.NewsId = 0;
    $scope.NewsDetail;
    $scope.NewsContent;

    var curHref = $window.location.href.toLowerCase().split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0] == 'id') {
                    $scope.NewsId = param[1];
                    laUserService.QueryDyNewsDetail($scope.NewsId, function (dataBack, status) {
                        $scope.NewsDetail = dataBack.Result;
                        $document[0].title = $scope.NewsDetail.NewTitle;
                        $scope.NewsContent = $sce.trustAsHtml(decodeURI($scope.NewsDetail.NewInfoDetail));
                    });
                    break;
                }
            }
        }
    }

}]);

laAir.controller('laAir_MoreInfo_UpgradePageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "系统升级";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

/**
 * Created by Jerry on 16/2/24.
 */

laAir.controller('laAir_PopDestination_XiamenPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "在厦门体验极致慢生活";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

laAir.controller('laAir_PopDestination_DalianPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "盛夏新航线";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

laAir.controller('laAir_PopDestination_HangBeiPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "杭州=北京换季时刻";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

laAir.controller('laAir_PopDestination_NanjingPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "傣家人的乐土";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);
laAir.controller('laAir_PopDestination_KaiLiPageCtl', ['$filter', '$document', '$scope', '$window', 'laUserService', 'laFlightService', 'laGlobalLocalService', function ($filter, $document, $scope, $window, laUserService, laFlightService, laGlobalLocalService) {

    $scope.title = "杭州-凯里-成都 购票抽好礼";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isSelfSrvNav = true;
    $scope.btnQueryTicketClick = function (DCity,ACity,DCityCode,ACityCode) {
        var td = new Date();
        td = new Date(td.setDate(td.getDate() + 1));
        var endtd=new Date(td.setDate(td.getDate() + 1));
        var tdmm = (parseInt(td.getMonth() + 1)).toString();
        var tdday = td.getDate().toString();
        tdmm = (tdmm.length < 2) ? '0' + tdmm : tdmm;
        tdday = (tdday.length < 2) ? '0' + tdday : tdday;
        var endtdmm = (parseInt(endtd.getMonth() + 1)).toString();
        var endtdday = endtd.getDate().toString();
        endtdmm = (endtdmm.length < 2) ? '0' + endtdmm : endtdmm;
        endtdday = (endtdday.length < 2) ? '0' + endtdday : endtdday;
        var qu_stime=td.getFullYear() + '-' + tdmm + '-' + tdday;
        var qu_endtime=endtd.getFullYear() + '-' + endtdmm + '-' + endtdday;
        var fli = new laEntityFlight();
        fli.AirportFrom = DCityCode;
        fli.AirportTo = ACityCode;
        fli.AirportFromCH = DCity;
        fli.AirportToCH = ACity;
        fli.DepartureTime = qu_stime;
        fli.RoundTripTime = qu_endtime;
        fli.RoundTrip = false;

        laGlobalLocalService.writeCookie(laGlobalProperty.laServiceConst_TransData_QueryTicket, JSON.stringify(fli), 0);
        $window.location.href = '/ETicket/AirlineList.html';
    };

}]);

laAir.controller('laAir_PopDestination_LijiangPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "壮丽东南第一州";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);
laAir.controller('laAir_PopDestination_YinChuanPageCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "黔行·山那边的寨子";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);
laAir.controller('laAir_PopDestination_RecruitInfoCtl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "长龙航空成熟乘务员招聘";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);
/**问卷调查第一页**/
laAir.controller('laAir_PopDestination_wenjuan1Ctl', ['$document', '$scope', function ($document, $scope) {

    $scope.title = "问卷调查";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

}]);

laAir.controller('laAir_PopDestination_wenjuanCtl', ['$filter', '$document', '$scope', '$window', 'laUserService', 'laFlightService', 'laGlobalLocalService', function ($filter, $document, $scope, $window, laUserService, laFlightService, laGlobalLocalService) {

    $scope.title = "问卷调查";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;
    var td = new Date();
    td = new Date(td.setDate(td.getDate()));
    var tdmm = (parseInt(td.getMonth() + 1)).toString();
    var tdday = td.getDate().toString();

    tdmm = (tdmm.length < 2) ? '0' + tdmm : tdmm;
    tdday = (tdday.length < 2) ? '0' + tdday : tdday;
    $("#planeTime").val(td.getFullYear() + '-' + tdmm + '-' + tdday);
    /**
     * 提交按钮点击事件
     */
    $scope.btnQueryTicketClick = function () {
        var customName = $("#customName").val();
        var planeTime = $("#planeTime").val();
        var flightNo = $("#flightNo").val();
        var phone = $("#phone").val();
        var Cert = $("#Cert").val();
        var CertNo = $("#CertNo").val();

        $scope.qu_customName = customName;
        $scope.qu_planeTime = planeTime;
        $scope.qu_flightNo = flightNo;
        $scope.qu_phone = phone;
        $scope.qu_Cert = Cert;
        $scope.qu_CertNo = CertNo;

        //姓名为空
        if (laGlobalLocalService.CheckStringIsEmpty($scope.qu_customName)) {
            bootbox.alert('请输入姓名');
            return;
        }
        //航班时间为空
        if (laGlobalLocalService.CheckStringIsEmpty($scope.qu_planeTime)) {
            bootbox.alert('请选择航班时间');
            return;
        }
        //航班号为空
        if (laGlobalLocalService.CheckStringIsEmpty($scope.qu_flightNo)) {
            bootbox.alert('请输入航班号');
            return;
        }
        //手机号为空
        if (laGlobalLocalService.CheckStringIsEmpty($scope.qu_phone)|| !laGlobalLocalService.CheckStringLength($scope.qu_phone, 11)) {
            bootbox.alert('请输入正确的手机号');
            return;
        }
        //证件类型为空
        if (laGlobalLocalService.CheckStringIsEmpty($scope.qu_Cert)) {
            bootbox.alert('请选择证件类型');
            return;
        }
        //证件号码为空
        if (laGlobalLocalService.CheckStringIsEmpty($scope.qu_CertNo)) {
            bootbox.alert('请输入证件号码');
            return;
        }
        //身份证号码错误
        if($("#Cert").val()==1){
            if(!laGlobalLocalService.IdentityCodeValid($scope.qu_CertNo)){
                bootbox.alert('您的证件号码有误！请核对后再输入');
                return;
            }
        }

        var postdata={
            "UserAnswers": [],
            "Name": "",
            "FlightDate": "",
            "FlightNo": "",
            "MobilePhone": "",
            "FOIDType": 1,
            "FOIDNumber": ""
        };
        postdata.name=$("#customName").val();
        postdata.FlightDate=$("#planeTime").val();
        postdata.FlightNo="GJ"+$("#flightNo").val();
        postdata.MobilePhone=$("#phone").val();
        postdata.FOIDType=$("#Cert").val();
        postdata.FOIDNumber=$("#CertNo").val();

        var answer0={
            "QuestionIndex": 0,
            "QuestionAnswer": "",
            "Suggestions": []
        };

        $("#buyWay input").each(function(){
            if(this.checked)
            {
                answer0.QuestionAnswer= $(this).val();

                if(this.id=="qita")
                {
                    answer0.QuestionAnswer= $("#qitatext").val();
                }
            }
        });

        postdata.UserAnswers.push(answer0);



        $(".Question").each(function (i) {
            var answer={
                "QuestionIndex": 0,
                "QuestionAnswer": "",
                "Suggestions": []
            };
            answer.QuestionIndex=i+1;
            answer.QuestionAnswer= $(this).find('.question-item input[checked=checked]').val();

            if(answer.QuestionIndex==13)
            {
                answer.QuestionAnswer= $("#question13").val();
            }

            $(this).find(".gaijin input").each(function(){
                if(this.checked)
                {
                    answer.Suggestions.push($(this).val());
                }
            });

            postdata.UserAnswers.push(answer);
        });
        //少答、漏答提示
        if(postdata.UserAnswers[0].QuestionAnswer==""||postdata.UserAnswers[0].QuestionAnswer==undefined){
            bootbox.alert('您有必答题忘记回答了');
            return;
        }else if(postdata.UserAnswers[12].QuestionAnswer==""||postdata.UserAnswers[12].QuestionAnswer==undefined){
            bootbox.alert('您有必答题忘记回答了');
            return;
        }else{
            for( var j=3;j<10;j++){
                if(postdata.UserAnswers[j].QuestionAnswer==""||postdata.UserAnswers[j].QuestionAnswer==undefined){
                    bootbox.alert('您有必答题忘记回答了');
                    return;
                }
                if(postdata.UserAnswers[j].QuestionAnswer==3||postdata.UserAnswers[j].QuestionAnswer==4||postdata.UserAnswers[j].QuestionAnswer==5){
                    if(postdata.UserAnswers[j].Suggestions.length==0){
                        bootbox.alert('您有必答题的改进意见忘记回答了');
                        return;
                    }

                }
            }
        }
        laUserService.SendQuestionnaireAnswer(postdata, function (dataBack, status) {
            if(dataBack.Code=='0000'){
                bootbox.alert(dataBack.Message);
            }
            else{
                bootbox.alert(dataBack.Message);
            }
        })
    };


}]);

laAir.controller('laAir_PopDestination_ThreeYearPageCtl', ['$filter', '$document', '$scope', '$window', 'laUserService', 'laGlobalLocalService', function ($filter, $document, $scope, $window, laUserService, laGlobalLocalService) {

    $scope.title = "3周年专享";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;
    //大图列表
    $scope.ThreeYearFlight;
    laUserService.QueryThreeYearFlightList(function (dataBack, status) {
        $scope.ThreeYearFlight = dataBack;
    });

    /*
     机票查询
     */
    $scope.btnThreeYearTicketQuery = function (tic) {
        var fli = new laEntityFlight();
        fli.AirportFrom = tic.DepartureAirportCode;
        fli.AirportTo = tic.ArriveAirportCode;
        fli.AirportFromCH = tic.DepartureAirportCH;
        fli.AirportToCH = tic.ArriveAirportCH;
        fli.DepartureTime = $filter('date')(tic.DepartureTime, 'yyyy-MM-dd');
        fli.RoundTripTime = "";
        fli.RoundTrip = false;

        laGlobalLocalService.writeCookie(laGlobalProperty.laServiceConst_TransData_QueryTicket, JSON.stringify(fli), 0);
        $window.location.href = '/ETicket/AirlineList.html';
    };
}]);

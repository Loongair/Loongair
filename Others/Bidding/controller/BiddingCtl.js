/**
 * Created by Jerry on 16/2/24.
 */

laAir.controller('laAir_Bidding_BiddingPageCtl', ['$sce', '$window', '$document', '$scope', 'laUserService', function ($sce, $window, $document, $scope, laUserService) {

    $scope.title = "招投标";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;

    $scope.BiddingId = 0;
    $scope.BiddingDetail;
    $scope.BiddingContent;
    $scope.queryFromLocal = false;

    var curHref = $window.location.href.toLowerCase().split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0] == 'id') {
                    $scope.BiddingId = param[1];

                    laUserService.QueryBiddingList(function (dataBack, status) {
                        var BiddingList = dataBack.bidList;
                        var n = BiddingList.length;
                        for (var i = 0; i < n; i++) {
                            var bid = BiddingList[i];
                            if (bid.n == $scope.BiddingId) {
                                $document[0].title = bid.t;
                                $scope.queryFromLocal = true;
                                $("#divnewslocal").css("display", "block");
                                break;
                            }
                        }
                        if (!$scope.queryFromLocal) {
                            laUserService.QueryBiddingDetail($scope.BiddingId, function (dataBack, status) {
                                $scope.BiddingDetail = dataBack.Result;
                                $document[0].title = $scope.BiddingDetail.BiddingTitle;
                                $scope.BiddingContent = $sce.trustAsHtml(decodeURI($scope.BiddingDetail.NewInfoDetail));
                                $("#divnewsdy").css("display", "block");
                            })
                        }
                    });
                    break;
                }
            }
        }
    }

}]);

/**
 * Created by Jerry on 16/2/14.
 */
var refundOrder = angular.module('refundOrder', ['laAir', 'ngFileUpload']);
refundOrder.controller('laAir_ETicket_RefundOrderPageCtl', ['$sce', '$document', 'Upload', '$interval', '$filter', '$window', '$scope', 'laUserService', 'laOrderService', 'laGlobalLocalService', function ($sce, $document, Upload, $interval, $filter, $window, $scope, laUserService, laOrderService, laGlobalLocalService) {

    $scope.title = "退票";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isSchTripNav = true;

    $scope.RefundTypeOptions = laEntityEnumrefundTypeOptions;
    $scope.InvoluntaryRefundOptions = laEntityEnumUnRefundOrderTypes;

    //订单信息
    $scope.orderInfo;
    $scope.ordId;
    //$scope.CanRefund = false;

    $scope.Param;

    //选中的退票乘机人
    $scope.checkedPsg = new Array();
    //退票联系人
    $scope.contactInfo = new laEntityContacts();

    $scope.refund = new laEntityRefundOrder();
    $scope.refund.RefundType = 2;
    $scope.refund.InvoluntaryRefundType = 1;
    $scope.refund.file = null;

    $scope.isRefunding = false;
    $scope.CanRefundPsgCnt = 0;

    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'ordid') {
                    $scope.ordId = param[1];
                    break;
                }
                if (param[0].toLowerCase() == 'param') {   //表示根据参数未登录查订单信息
                    try {
                        //$scope.Param = JSON.parse(new Base64().decode(param[1]));
                        $scope.Param = JSON.parse(new Base64().decode(params[i].substr(6)));
                    } catch (e) {

                    }

                    break;
                }
            }
        }
    }
    if ($scope.ordId != undefined && $scope.ordId != null) {
        getOrderInfo($scope.ordId);
    }
    if ($scope.Param != undefined && $scope.Param != null) {
        getOrderInfoWithoutLogin();
    }

    $scope.btnChooseRefundType = function () {
        if ($scope.refund.RefundType == 2) {
            $("#divRefundNote").hide();
            $("#divRefundOrderType").hide();
            $("#divRefundFile").hide();
            $scope.contactInfo.Note = "";
            return;
        }
        if ($scope.refund.RefundType == 3) {
            $("#divRefundOrderType").show();
            $("#divRefundFile").show();
            $("#divRefundNote").show();
            return;
        }
        if ($scope.refund.RefundType == 6) {
            $("#divRefundOrderType").hide();
            $("#divRefundFile").hide();
            $("#divRefundNote").show();
            return;
        }
    };

    /**
     * 判断乘机人是否可以退票
     * @param psg
     */
    $scope.psgCanRefund = function (psg) {
        var result = false;
        var flist = psg.Flights;
        var n = flist.length;
        for (var i = 0; i < n; i++) {
            var item = flist[i];
            if (item.CanRefund == true || item.CanErrorRefund == true || item.CanInvoluntary == true) {
                result = true;
                break;
            }
        }
        if (result) {
            $scope.CanRefundPsgCnt++;
        }
        return result;
    };

    $scope.createTGQHtml = function getTGQHtml(flipsg, orderType) {
        var insHtml = "";
        //var nF = psg.Flights.length;

        var fli = flipsg;
        var carbin = fli;
        var reRule = carbin.RefundRule.split('-');
        var chRule = carbin.ChangeRule.split('-');
        var reRuleJSON = {"b": {"p": "", "a": ""}, "a": {"p": "", "a": ""}};
        var chRuleJSON = {"b": {"p": "", "a": ""}, "a": {"p": "", "a": ""}};
        if (reRule.length >= 3) {
            if (laGlobalLocalService.IsNum(reRule[0])) {
                var am = parseInt((reRule[0] / carbin.SaleTicketPrice) * 100);
                reRuleJSON.b.p = (reRule[0] > 0) ? am.toString() + "%" : "";
                reRuleJSON.b.a = (reRule[0] > 0) ? "(" + reRule[0].toString() + "元)" : reRule[0].toString() + "元";
            } else {
                reRuleJSON.b.p = reRule[0];
            }
            if (laGlobalLocalService.IsNum(reRule[2])) {
                var am = parseInt((reRule[2] / carbin.SaleTicketPrice) * 100);
                reRuleJSON.a.p = (reRule[2] > 0) ? am.toString() + "%" : "";
                reRuleJSON.a.a = (reRule[2] > 0) ? "(" + reRule[2].toString() + "元)" : reRule[2].toString() + "元";
            } else {
                reRuleJSON.a.p = reRule[2];
            }
        }
        if (chRule.length >= 3) {
            if (laGlobalLocalService.IsNum(chRule[0])) {
                var am = parseInt((chRule[0] / carbin.SaleTicketPrice) * 100);
                chRuleJSON.b.p = (chRule[0] > 0) ? am.toString() + "%" : "";
                chRuleJSON.b.a = (chRule[0] > 0) ? "(" + chRule[0].toString() + "元)" : chRule[0].toString() + "元";
            } else {
                chRuleJSON.b.p = chRule[0];
            }
            if (laGlobalLocalService.IsNum(chRule[2])) {
                var am = parseInt((chRule[2] / carbin.SaleTicketPrice) * 100);
                chRuleJSON.a.p = (chRule[2] > 0) ? am.toString() + "%" : "";
                chRuleJSON.a.a = (chRule[2] > 0) ? "(" + chRule[2].toString() + "元)" : chRule[2].toString() + "元";
            } else {
                chRuleJSON.a.p = chRule[2];
            }
        }
        var note = "</br>如自愿要求变更或退票，按优惠前公布运价(" + carbin.TicketPrice + "元)收取手续费";

        if (5 == orderType) {
            note = "退票只退票面及税费差价，如自愿要求变更或退票，按优惠前公布运价(" + carbin.TicketPrice + "元)收取手续费</br>原订单退改请参考原订单退改规则";
        }

        insHtml = "<div><table style='width:100%;' cellpadding='0' cellspacing='0'><tr><td style='text-align: right;'>类型</td><td style='text-align: left;'>起飞前2小时前</td><td style='text-align: left;'>起飞前2小时后</td></tr>" +
            "<tr><td style='text-align: right;'>退票手续费</td><td style='text-align: left;color: orange;'>" + reRuleJSON.b.p + reRuleJSON.b.a + "</td><td style='text-align: left;color: orange;'>" + reRuleJSON.a.p + reRuleJSON.a.a + "</td></tr>" +
            "<tr><td style='text-align: right;'>同舱改期收费</td><td style='text-align: left;color: orange;'>" + chRuleJSON.b.p + chRuleJSON.b.a + "</td><td style='text-align: left;color: orange;'>" + chRuleJSON.a.p + chRuleJSON.a.a + "</td></tr>" +
            "<tr><td style='text-align: right;'>说明</td><td style='text-align: left;color: orange;' colspan='2'>签转条件:" + carbin.SignedTransfer + "</br>" + note + "</td></tr>" +
            "</table></div>";

        return $sce.trustAsHtml(insHtml);
    };

    /**
     * 退票
     */
    $scope.btnRefundClick = function () {
        if ($scope.orderInfo.PayStatus == 2) {// && $scope.CanRefund
            if (laGlobalLocalService.CheckStringIsEmpty($scope.contactInfo.ContactsName)) {
                bootbox.alert('请填写退票联系人姓名');
                return;
            }
            if (laGlobalLocalService.CheckStringIsEmpty($scope.contactInfo.ContactsMobile) || !laGlobalLocalService.CheckStringLength($scope.contactInfo.ContactsMobile, 11)) {
                bootbox.alert('请填写11位退票联系人手机');
                return;
            }
            if ($scope.refund.RefundType == 3 && laGlobalLocalService.CheckStringIsEmpty($scope.contactInfo.Note)) {
                bootbox.alert('非自愿退票请填写退票原因');
                return;
            }
            if ($scope.refund.RefundType == 6 && laGlobalLocalService.CheckStringIsEmpty($scope.contactInfo.Note)) {
                bootbox.alert('补退退票请填写退票原因');
                return;
            }

            //var refund = new laEntityRefundOrder();
            $scope.refund.OrderId = $scope.orderInfo.OrderId;
            $scope.refund.Note = ($scope.contactInfo.Note == undefined) ? '' : $scope.contactInfo.Note;
            $scope.refund.ContactsName = $scope.contactInfo.ContactsName;
            $scope.refund.ContactsEMail = ($scope.contactInfo.ContactsEMail) ? '' : $scope.contactInfo.ContactsEMail;
            $scope.refund.ContactsMobile = $scope.contactInfo.ContactsMobile;
            $scope.refund.RefundAmount = 0;

            var handleFee = 0;
            for (var i = 0; i < $scope.checkedPsg.length; i++) {
                if ($scope.checkedPsg[i] != 0 && !laGlobalLocalService.CheckStringIsEmpty($scope.checkedPsg[i])) {
                    for (var n = 0; n < $scope.orderInfo.Passengers.length; n++) {
                        if ($scope.checkedPsg[i] == $scope.orderInfo.Passengers[n].PassengerId) {
                            var psg = new laEntityRefundOrderPassenger();
                            for (var l = 0; l < $scope.orderInfo.Passengers[n].Flights.length; l++) {
                                var ticketPrice = $scope.orderInfo.OrderType == 5 ? $scope.orderInfo.Passengers[n].Flights[l].TicketPriceDifference : $scope.orderInfo.Passengers[n].Flights[l].SaleTicketPrice;
                                var singleHandle = 0;

                                if ($scope.refund.RefundType == 2) {
                                    if ($scope.orderInfo.OrderType == 5) {
                                        singleHandle += $scope.orderInfo.Passengers[n].Flights[l].OldTicketHandleAmount;
                                    }
                                }

                                if ($scope.refund.RefundType == 2 || $scope.orderInfo.OrderType == 5) {
                                    singleHandle += getRefundHandleFee($scope.orderInfo.Passengers[n].Flights[l].RefundRule, $scope.orderInfo.Passengers[n].Flights[l].DepartureTime, ticketPrice)
                                }
                                handleFee += singleHandle;
                                psg.FlightIds[l] = $scope.orderInfo.Passengers[n].Flights[l].FlightId;
                                if ($scope.refund.RefundType == 2) {
                                    if ($scope.orderInfo.Passengers[n].Flights[l].CanRefund == false) {
                                        bootbox.alert('乘机人:' + $scope.orderInfo.Passengers[n].PassengerName +
                                            ',行程:' + $scope.orderInfo.Passengers[n].Flights[l].DepartureCityCH +
                                            '-' + $scope.orderInfo.Passengers[n].Flights[l].ArriveCityCH + ' 不允许自愿退票:' +
                                            $scope.orderInfo.Passengers[n].Flights[l].CanNotRefundNote);
                                        return;
                                    }
                                }
                                if ($scope.refund.RefundType == 3) {
                                    if ($scope.orderInfo.Passengers[n].Flights[l].CanInvoluntary == false) {
                                        bootbox.alert('乘机人:' + $scope.orderInfo.Passengers[n].PassengerName +
                                            ',行程:' + $scope.orderInfo.Passengers[n].Flights[l].DepartureCityCH +
                                            '-' + $scope.orderInfo.Passengers[n].Flights[l].ArriveCityCH + ' 不允许非自愿退票:' +
                                            $scope.orderInfo.Passengers[n].Flights[l].CanNotInvoluntaryNote);
                                        return;
                                    }
                                }
                                if ($scope.refund.RefundType == 6) {
                                    if ($scope.orderInfo.Passengers[n].Flights[l].CanErrorRefund == false) {
                                        bootbox.alert('乘机人:' + $scope.orderInfo.Passengers[n].PassengerName +
                                            ',行程:' + $scope.orderInfo.Passengers[n].Flights[l].DepartureCityCH +
                                            '-' + $scope.orderInfo.Passengers[n].Flights[l].ArriveCityCH + ' 不允许补退:' +
                                            $scope.orderInfo.Passengers[n].Flights[l].CanNotErrorRefundNote);
                                        return;
                                    }
                                }
                            }
                            psg.PassengerId = $scope.checkedPsg[i];

                            //$scope.refund.Passengers[i] = psg;
                            $scope.refund.Passengers[$scope.refund.Passengers.length] = psg;
                        }

                    }
                }
            }

            if ($scope.refund.Passengers.length <= 0) {
                bootbox.alert('请选择要退票的乘机人');
                return;
            }

            if ($scope.refund.RefundType == 3 && $scope.refund.InvoluntaryRefundType == 3 && $("input[type='file']").val() == "") {
                bootbox.alert('非自愿退票-因病退票必须上传证明文件！');
                return;
            } else if ($scope.refund.RefundType == 3 && $("input[type='file']").val() != "") {
                for (var j = 0; j < $scope.refund.file.length; j++) {
                    var filePath = $scope.refund.file[j].name;
                    var fileExtention = filePath.substring(filePath.lastIndexOf(".")).toLowerCase();
                    if (!fileExtention.match(/.jpg|.png|.doc|.pdf/i)) {
                        bootbox.alert('请选择正确格式的文件（jpg、png、doc、pdf）');
                        return true;
                    }
                }
            }

            var note = "";
            if ($scope.refund.RefundType != 6) {
                if ($scope.orderInfo.OrderType == 5) {
                    note += "温馨提示：改期前订单将一同做退票处理。";
                }
                note += "预计手续费金额为" + handleFee + "元(手续费仅供参考，实际以后台审核为准，谢谢)。";
            }

            bootbox.confirm(note + '您是否要退票?', function (result) {
                if (result) {
                    $scope.isRefunding = true;
                    $scope.timeDown = 1;
                    if ($scope.refund.RefundType == 3 && $("input[type='file']").val() != "") {
                        submitRefundInfoWithFile();
                    }
                    else {
                        submitRefundInfo();
                    }

                }
                else {
                    $scope.refund.Passengers = new Array();
                }
            })
        } else {
            bootbox.alert('该订单不能退票,只有已支付的订单且有未退票的航段才可以退票');
        }
    }

    function submitRefundInfoWithFile() {
        Upload.upload({
            //服务端接收
            url: '/Flight/UploadFile',
            //上传的同时带的参数
            data: {file: $scope.refund.file, 'orderId': $scope.orderInfo.OrderId},
        }).success(function (data, status, headers, config) {
            if (data.Code == "0000") {
                $scope.refund.FileGuid = data.FileGuid;
                submitRefundInfo();
            }
            else {
                bootbox.alert('文件上传失败: ' + data.Message, function () {
                    $window.location.href = "/ETicket/RefundOrder.html?ordId=" + $scope.orderInfo.OrderId;
                });
            }

        }).error(function (data, status, headers, config) {
            bootbox.alert('文件上传失败！', function () {
                $window.location.href = "/ETicket/RefundOrder.html?ordId=" + $scope.orderInfo.OrderId;
            });
        });
    }

    function submitRefundInfo() {
        if ($scope.ordId != undefined && $scope.ordId != null) {
            laOrderService.RefundOrder($scope.refund, function (backData, status) {
                var rs = backData;
                if (rs.Code == laGlobalProperty.laServiceCode_Success) {
                    var timer = $interval(function () {
                        $scope.timeDown = $scope.timeDown - 1;
                        if ($scope.timeDown <= 0) {
                            $interval.cancel(timer);
                            $scope.isRefunding = false;
                            bootbox.alert('提交成功', function () {
                                $window.location.href = "/ETicket/OrderDetail.html?ordId=" + $scope.orderInfo.OrderId;
                            });
                        }
                    }, 1000);
                } else {
                    var timer = $interval(function () {
                        $scope.timeDown = $scope.timeDown - 1;
                        if ($scope.timeDown <= 0) {
                            $interval.cancel(timer);
                            $scope.isRefunding = false;
                            bootbox.alert(rs.Message, function () {
                            });
                        }
                    }, 1000);
                }
            });
        }
        if ($scope.Param != undefined && $scope.Param != null) {
            $scope.refund.sessionId = $scope.Param.sessionId;
            $scope.refund.SignKey = $scope.orderInfo.SignKey;
            laOrderService.RefundOrderWithoutLogin($scope.refund, function (backData, status) {
                var rs = backData;
                if (rs.Code == laGlobalProperty.laServiceCode_Success) {
                    var timer = $interval(function () {
                        $scope.timeDown = $scope.timeDown - 1;
                        if ($scope.timeDown <= 0) {
                            $interval.cancel(timer);
                            $scope.isRefunding = false;
                            bootbox.alert('提交成功！', function () {
                                $window.location.href = "/ETicket/OrderDetail.html?ordId=" + $scope.orderInfo.OrderId;
                            });
                        }
                    }, 1000);
                } else {
                    var timer = $interval(function () {
                        $scope.timeDown = $scope.timeDown - 1;
                        if ($scope.timeDown <= 0) {
                            $interval.cancel(timer);
                            $scope.isRefunding = false;
                            bootbox.alert(rs.Message, function () {
                            });
                        }
                    }, 1000);
                }
            });
        }
    }

    $scope.btnChangeClick = function () {
        bootbox.alert("因线上改签仍在开发中，如需改签请致电客服0571-89999999", function () {
        });
    };

    /**
     * 查询订单信息
     * @param ordId
     */
    function getOrderInfo(ordId) {
        laOrderService.QueryOrderInfo(ordId, function (backData, status) {
            var rs = backData;
            if (rs.Code == laGlobalProperty.laServiceCode_Success) {
                $scope.orderInfo = rs;
                /*
                 psgloop:
                 for (var i = 0; i < $scope.orderInfo.Passengers.length; i++) {
                 for (var n = 0; n < $scope.orderInfo.Passengers[i].Flights.length; n++) {
                 if ($scope.orderInfo.Passengers[i].Flights[n].CanRefund) {
                 $scope.CanRefund = true;
                 break psgloop;
                 }
                 }
                 }
                 */
                $scope.contactInfo.ContactsName = rs.Contacts.ContactsName;
                $scope.contactInfo.ContactsMobile = rs.Contacts.ContactsMobile;
                $scope.contactInfo.ContactsEMail = (laGlobalLocalService.CheckStringIsEmpty(rs.Contacts.ContactsEMail)) ? '' : rs.Contacts.ContactsEMail;

                for (var i = 0; i < $scope.orderInfo.Passengers.length; i++) {
                    $scope.checkedPsg[i] = 0;
                }
            }
        });
    }

    function getOrderInfoWithoutLogin() {
        laOrderService.QueryOrderInfoWithoutLogin($scope.Param.ordid, $scope.Param.mobile, $scope.Param.verifycode, $scope.Param.sessionId, function (backData, status) {
            var rs = backData;
            if (rs.Code == laGlobalProperty.laServiceCode_Success) {
                $scope.orderInfo = rs;
                /*
                 psgloop:
                 for (var i = 0; i < $scope.orderInfo.Passengers.length; i++) {
                 for (var n = 0; n < $scope.orderInfo.Passengers[i].Flights.length; n++) {
                 if ($scope.orderInfo.Passengers[i].Flights[n].CanRefund) {
                 $scope.CanRefund = true;
                 break psgloop;
                 }
                 }
                 }
                 */
                $scope.contactInfo.ContactsName = rs.Contacts.ContactsName;
                $scope.contactInfo.ContactsMobile = rs.Contacts.ContactsMobile;
                $scope.contactInfo.ContactsEMail = (laGlobalLocalService.CheckStringIsEmpty(rs.Contacts.ContactsEMail)) ? '' : rs.Contacts.ContactsEMail;

                for (var i = 0; i < $scope.orderInfo.Passengers.length; i++) {
                    $scope.checkedPsg[i] = 0;
                }
            }
        });
    }

    function getRefundHandleFee(rule, departureTime, price) {
        var handleFee = 0;
        var reRule = rule.split('-');
        var handleArray = new Array();
        var timeArray = new Array();
        var handleIndex = 0;
        var timeIndex = 0;
        for (var i = 0; i < reRule.length; i += 2) {
            var ruleArray = reRule[i].split(/[(,)]/);
            handleArray[handleIndex++] = ruleArray[1];
            if (i + 1 < reRule.length) {
                timeArray[timeIndex++] = reRule[i + 1];
            }
        }

        var dt = new Date();
        var dtSpan = Date.UTC(dt.getUTCFullYear(), dt.getUTCMonth(), dt.getUTCDate(), dt.getUTCHours(), dt.getUTCMinutes(), dt.getUTCSeconds());
        for (i = 0; i < timeArray.length; i++) {
            var flightSpan = Date.UTC(departureTime.substr(0, 4), departureTime.substr(5, 2) - 1, departureTime.substr(8, 2), departureTime.substr(11, 2), departureTime.substr(14, 2), departureTime.substr(17, 2)) - 8 * 3600000 - (parseInt(timeArray[i]) * 360000);
            if (flightSpan > dtSpan) {
                return parseInt(handleArray[i]);
            }

            if ((i + 1) == timeArray.length) {
                return parseInt(handleArray[i + 1]);
            }
        }

        return price;
    }

}])
;

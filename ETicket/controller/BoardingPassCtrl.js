/**
 * Created by cy on 2016-12-22.
 */
laAir.controller('laAir_ETicket_BoardingPassPageCtl', ['$filter', '$interval', '$document', '$window', '$scope', 'laUserService', 'laGlobalLocalService', function ($filter, $interval, $document, $window, $scope, laUserService, laGlobalLocalService) {
    $scope.title = "电子登机牌下载";
    $document[0].title = $scope.title;
   /* public string ENName;               //旅客姓名（拼音或者英文）
    public string CNName;               //旅客姓名中文
    public string FlightNum;            //航班号
    public string DepartureCityEN;      //起飞城市拼音
    public string ArrivalCityEN;            //达到城市拼音

    public string DepartureCityCn;              //起飞城市中文名称
    public string ArrivalCityCN;                //到达城市中文名称
    public string TicketNum;                    //票号
    public string CabinClass;                   //舱位
    public string Gate;                     //登机口

    public string BoardingTime;             //登机时间(例如1830)
    public string Date;                       //日期（例如15DEC）
    public string SearNum;                      //座位号
    public string ebpImgByteStr;                //base64编码后的图片数据流
    public string SerialNo;                     //序号*/
    $scope.ENName;
    $scope.CNName;
    $scope.FlightNum;
    $scope.DepartureCityEN;
    $scope.ArrivalCityEN;

    $scope.DepartureCityCn;
    $scope.ArrivalCityCN;
    $scope.TicketNum;
    $scope.CabinClass;
    $scope.Gate;

    $scope.BoardingTime;
    $scope.FlightDate;
    $scope.SearNum;
    $scope.ebpImgByteStr;
    $scope.SerialNo;

    $scope.Certificate;
    $scope.CertificateCH;
    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'param') {
                    try {
                        $scope.Certificate = JSON.parse(new Base64().decode(params[i].substr(6)));
                    }
                    catch (e) {

                    }
                    break;
                }
            }
        }
    }
    if ($scope.Certificate != undefined && $scope.Certificate != null) {
        var chkinfo = {
            "FromCity": $scope.Certificate.FromCity,
            "ToCity": $scope.Certificate.ToCity,
            "TKTNumber": $scope.Certificate.TKTNumber
        };
        laUserService.DownLoadBoardingPass(chkinfo, function (backData, status) {
            if (backData.Code == laGlobalProperty.laServiceCode_Success) {

                $scope.CertificateCH = backData;

                $scope.ENName=$scope.CertificateCH.Result.ENName;
                $scope.CNName=$scope.CertificateCH.Result.CNName;
                $scope.FlightNum=$scope.CertificateCH.Result.FlightNum;
                $scope.DepartureCityEN=$scope.CertificateCH.Result.DepartureCityEN;
                $scope.ArrivalCityEN=$scope.CertificateCH.Result.ArrivalCityEN;

                $scope.DepartureCityCn=$scope.CertificateCH.Result.DepartureCityCn;
                $scope.ArrivalCityCN=$scope.CertificateCH.Result.ArrivalCityCN;
                $scope.CabinClass=$scope.CertificateCH.Result.CabinClass;
                $scope.TicketNum=$scope.CertificateCH.Result.TicketNum;
                $scope.Gate=$scope.CertificateCH.Result.Gate;

                $scope.BoardingTime=$scope.CertificateCH.Result.BoardingTime;
                $scope.FlightDate=$scope.CertificateCH.Result.Date;
                $scope.SearNum=$scope.CertificateCH.Result.SearNum;
                $scope.ebpImgByteStr=$scope.CertificateCH.Result.ebpImgByteStr;
                $scope.SerialNo=$scope.CertificateCH.Result.SerialNo;

            } else {
                bootbox.alert(backData.Message);
            }
        });
    }

}]);
/**
 * Created by Jerry on 16/2/4.
 */

laAir.controller('laAir_MemberWeChatCallbackPageCtl', ['$document',  '$window', '$scope', '$interval', 'laUserService', 'laGlobalLocalService', function ($document, $window, $scope, $interval, laUserService, laGlobalLocalService) {

    $scope.title = "微信登录";
    $document[0].title = $scope.title;
    /**
     * 设置导航栏ClassName
     * @type {boolean}
     */
    $scope.isHomeNav = true;
    $scope.SessionID;
    $scope.Action;
    $scope.Code;
    $scope.State;
    $scope.WeChatNiName="";
    $scope.HeadImageUrl="";

    var curHref = $window.location.href.split('?');
    if (curHref.length >= 2) {
        var params = curHref[1].split('&');
        for (var i = 0; i < params.length; i++) {
            var param = params[i].split('=');
            if (param.length >= 2) {
                if (param[0].toLowerCase() == 'code') {
                        $scope.Code = param[1];
                }else if(param[0].toLowerCase() == 'state') {
                    $scope.State = param[1];
                }else if(param[0].toLowerCase() == 'action') {
                    $scope.Action = param[1];
                }

            }
        }
        // var postdata={
        //     Code:$scope.Code,
        //     State:$scope.State,
        //     Action:$scope.Action
        // };
        // laUserService.WeiXinLogin(postdata, function (dataBack, status) {
        //     var rs = dataBack;
        //     if (rs.Code == '0000') {
        //         $scope.WeChatNiName=rs.WeChatNiName;
        //         $scope.HeadImageUrl=rs.HeadImageUrl;
        //     }
        //     else {
        //         bootbox.alert(rs.Message);
        //     }
        // });
    }
    if($scope.Code==''||$scope.Code==undefined) {
        $window.location.href = "/B2C/home.html"
    }
    if($scope.Action=="wechatLogin"){
        var postdata={
            Code:$scope.Code,
            State:$scope.State
        };
        laUserService.WeiXinWeChatLogin(postdata, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == '0000') {
                $scope.WeChatNiName=rs.WeChatNiName;
                $scope.HeadImageUrl=rs.HeadImageUrl;
                if(rs.AccountExit){
                    var backUrl = laGlobalLocalService.getCookie(laGlobalProperty.laServiceConst_TransData_BackUrl);
                    if (backUrl != undefined) {
                        if (backUrl.toLowerCase().indexOf('/member/WeChatCallback.html', 0) >= 0) {
                            $window.location.href = '/B2C/home.html';
                        } else {
                            $window.location.href = backUrl;
                        }
                    } else {
                        $window.location.href = '/B2C/home.html';
                    }
                }else{
                    $window.location.href = "/Member/BindingInformation.html?param=" + new Base64().encode(JSON.stringify(rs));
                }
            }
            else {
                bootbox.alert(rs.Message);
            }
        });
    }else if($scope.Action=="wechatBinding"){

        var postdata={
            Code:$scope.Code,
            State:$scope.State
        };
        laUserService.WeiXinWeChatBinding(postdata, function (dataBack, status) {
            var rs = dataBack;
            if (rs.Code == '0000') {
                var backUrl = laGlobalLocalService.getCookie(laGlobalProperty.laServiceConst_TransData_BackUrl);
                if (backUrl != undefined) {
                    if (backUrl.toLowerCase().indexOf('/member/WeChatCallback.html', 0) >= 0) {
                        $window.location.href = '/B2C/home.html';
                    } else {
                        $window.location.href = backUrl;
                    }
                } else {
                    $window.location.href = '/B2C/home.html';
                }
            }
            else {
                bootbox.alert(rs.Message);
            }
        });
    }
}]);
